export interface Config {
	version: string
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
declare global {
	interface Window {
		config?: any
	}
}

function isConfig(object: any): object is Config {
	return typeof object === "object" && "version" in object
}

export function getConfig(): Config {
	if (isConfig(window.config)) {
		return window.config
	}
	return {
		version: "dev",
	}
}
