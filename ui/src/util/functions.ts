function secondsToString(seconds: number): string {
	const numyears = Math.floor(seconds / 31536000)
	const numdays = Math.floor((seconds % 31536000) / 86400)
	const numhours = Math.floor(((seconds % 31536000) % 86400) / 3600)
	const numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60)
	const numseconds = (((seconds % 31536000) % 86400) % 3600) % 60

	let res = []
	if (numyears > 0) res.push(numyears + "yr")
	if (numdays > 0) res.push(numdays + "d")
	if (numhours > 0) res.push(numhours + "h")
	if (numminutes > 0) res.push(numminutes + "m")
	if (seconds < 60) res.push(numseconds + "s")

	return res.join(" ")
}

export {secondsToString}
