class colors {
	static readonly RED = "#FF0039"
	static readonly GREEN = "#148420"
	static readonly BLUE = "#1f85de"
}

export default colors
