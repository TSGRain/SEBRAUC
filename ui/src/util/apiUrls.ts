import {Configuration, DefaultApi} from "../sebrauc-client"

let apiHost = document.location.host
const httpProto = document.location.protocol
const wsProto = httpProto === "https:" ? "wss:" : "ws:"

if (import.meta.env.VITE_API_HOST !== undefined) {
	apiHost = import.meta.env.VITE_API_HOST as string
}

const apiUrl = `${httpProto}//${apiHost}/api`
const wsUrl = `${wsProto}//${apiHost}/api/ws`

let apicfg = new Configuration({
	basePath: apiUrl,
})

const sebraucApi = new DefaultApi(apicfg)

export {apiUrl, wsUrl, sebraucApi}
