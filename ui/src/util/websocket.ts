import {wsUrl} from "./apiUrls"

class WebsocketAPI {
	private static ws: WebsocketAPI | undefined

	private conn: WebSocket | undefined
	private wsConnected: boolean

	private clients: Set<WebsocketClient>

	private constructor() {
		this.clients = new Set()
		this.wsConnected = false

		if (window.WebSocket) {
			this.connect()
		} else {
			console.log("Your browser does not support WebSockets")
		}
	}

	private setStatus(wsConnected: boolean) {
		if (wsConnected !== this.wsConnected) {
			this.wsConnected = wsConnected
			this.clients.forEach((client) => {
				client.statusCallback(this.wsConnected)
			})
		}
	}

	private connect() {
		this.conn = new WebSocket(wsUrl)
		this.conn.onopen = () => {
			this.setStatus(true)
			console.log("WS connected")
		}
		this.conn.onclose = () => {
			this.setStatus(false)
			console.log("WS connection closed")
			window.setTimeout(() => this.connect(), 3000)
		}
		this.conn.onmessage = (evt) => {
			this.clients.forEach((client) => {
				client.msgCallback(evt)
			})
		}
	}

	static Get(): WebsocketAPI {
		if (this.ws === undefined) {
			this.ws = new WebsocketAPI()
		}
		return this.ws
	}

	isConnected(): boolean {
		return this.wsConnected
	}

	addClient(client: WebsocketClient) {
		console.log("added client", client)
		this.clients.add(client)
	}

	removeClient(client: WebsocketClient) {
		console.log("removed client", client)
		this.clients.delete(client)
	}
}

export default class WebsocketClient {
	statusCallback: (wsConnected: boolean) => void
	msgCallback: (evt: MessageEvent) => void

	constructor(
		statusCallback: (wsConnected: boolean) => void,
		msgCallback: (evt: MessageEvent) => void
	) {
		this.statusCallback = statusCallback
		this.msgCallback = msgCallback

		this.api().addClient(this)
	}

	api(): WebsocketAPI {
		return WebsocketAPI.Get()
	}

	destroy() {
		this.api().removeClient(this)
	}
}
