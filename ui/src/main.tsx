import {render} from "preact"
import App from "./components/app"
import "./style/index.scss"

render(<App />, document.getElementById("app")!)
