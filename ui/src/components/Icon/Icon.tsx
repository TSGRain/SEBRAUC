import {Component} from "preact"
import "./Icon.scss"

type Props = {
	icon: string
	size: number
	color?: string
}

export default class Icon extends Component<Props> {
	static defaultProps = {
		size: 24,
	}

	render() {
		return (
			<span class="icon" style={{color: this.props.color}}>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					style={`height: ${this.props.size}px; width: ${this.props.size}px;`}
					viewBox="0 0 24 24"
					aria-hidden="true"
				>
					<path d={this.props.icon} />
				</svg>
			</span>
		)
	}
}
