import {Component, createRef, JSX, ComponentChild, ComponentChildren} from "preact"
import "./Dropzone.scss"

type Props = {
	disabled: boolean
	clickable: boolean
	multiple: boolean
	accept?: string
	onFilesAdded?: (files: File[]) => void

	children?: ComponentChild | ComponentChildren
}

type State = {
	highlight: boolean
}

export default class Dropzone extends Component<Props, State> {
	static defaultProps = {
		disabled: false,
		clickable: false,
		multiple: false,
	}

	private fileInputRef = createRef<HTMLInputElement>()

	openFileDialog() {
		this.fileInputRef.current?.click()
	}

	reset() {
		const input = this.fileInputRef.current
		if (input === null) return
		input.value = ""
	}

	private onClick = () => {
		if (this.props.disabled || !this.props.clickable) return
		this.openFileDialog()
	}

	private onFilesAdded = (evt: JSX.TargetedEvent) => {
		if (this.props.disabled || evt.target === null) return

		const input = evt.target as HTMLInputElement
		const files = input.files

		if (this.props.onFilesAdded) {
			const array = this.fileListToArray(files)
			this.props.onFilesAdded(array)
		}
	}

	private onDragOver = (evt: DragEvent) => {
		evt.preventDefault()

		if (this.props.disabled) return

		this.setState({highlight: true})
	}

	private onDragLeave = () => {
		this.setState({highlight: false})
	}

	private onDrop = (evt: DragEvent) => {
		evt.preventDefault()
		this.setState({highlight: false})

		if (this.props.disabled || evt.dataTransfer === null) return

		const files = evt.dataTransfer.files

		if (!this.props.multiple && files.length > 1) return

		if (this.props.onFilesAdded) {
			const array = this.fileListToArray(files)
			this.props.onFilesAdded(array)
		}
	}

	private fileListToArray(list: FileList | null): File[] {
		const array: File[] = []
		if (list === null) return array

		for (var i = 0; i < list.length; i++) {
			array.push(list.item(i)!)
		}
		return array
	}

	render() {
		return (
			<div
				class={`dropzone ${this.state.highlight ? "highlight" : ""}`}
				onDragOver={this.onDragOver}
				onDragLeave={this.onDragLeave}
				onDrop={this.onDrop}
				onClick={this.onClick}
				style={{
					cursor:
						!this.props.disabled && this.props.clickable
							? "pointer"
							: "default",
				}}
			>
				<input
					ref={this.fileInputRef}
					class="fileholder"
					type="file"
					multiple={this.props.multiple}
					accept={this.props.accept}
					onInput={this.onFilesAdded}
				/>
				{this.props.children}
			</div>
		)
	}
}
