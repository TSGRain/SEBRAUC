import {Component} from "preact"
import UpdaterView from "./Updater/UpdaterView"
import logo from "../assets/logo.svg"
import {getConfig} from "../util/config"

export default class App extends Component {
	render() {
		return (
			<div>
				<img src={logo} alt="SEBRAUC" height="64" />
				{getConfig().version}
				<UpdaterView />
			</div>
		)
	}
}
