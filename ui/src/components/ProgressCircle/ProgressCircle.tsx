import {Component, ComponentChild, ComponentChildren} from "preact"
import "./ProgressCircle.scss"

type Props = {
	ready: boolean
	progress: number
	color: string

	children?: ComponentChild | ComponentChildren
}

export default class ProgressCircle extends Component<Props> {
	static defaultProps = {
		ready: false,
		progress: 0,
		color: "#FDB900",
	}

	render() {
		const percentage = this.props.ready ? 0 : this.props.progress
		const visible = !this.props.ready && this.props.progress > 0

		return (
			<div class="progress-box">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
					<circle cx="50" cy="50" r="45" fill={this.props.color} />

					{visible ? (
						<path
							class="progress-path"
							stroke-linecap="round"
							stroke-width="5"
							stroke="#fff"
							fill="none"
							d="M50 10
			   a 40 40 0 0 1 0 80
			   a 40 40 0 0 1 0 -80"
							stroke-dasharray={`${percentage * 2.512}, 251.2`}
						/>
					) : null}

					{visible ? (
						<text
							id="count"
							x="50"
							y="50"
							text-anchor="middle"
							dy="7"
							font-size="20"
							fill="#fff"
						>
							{percentage}%
						</text>
					) : null}
				</svg>
				{visible ? null : this.props.children}
			</div>
		)
	}
}
