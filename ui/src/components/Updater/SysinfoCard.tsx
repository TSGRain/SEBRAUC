import {Component} from "preact"
import {SystemInfo} from "../../sebrauc-client"
import {sebraucApi} from "../../util/apiUrls"
import {secondsToString} from "../../util/functions"
import Icon from "../Icon/Icon"
import {
	mdiAlphaVCircleOutline,
	mdiCheckCircleOutline,
	mdiCircleOutline,
	mdiClockOutline,
	mdiCloseCircleOutline,
	mdiMonitor,
	mdiPenguin,
	mdiTagMultipleOutline,
	mdiTagOutline,
} from "@mdi/js"
import colors from "../../util/colors"

type Props = {}

type State = {
	sysinfo: SystemInfo
}

export default class SysinfoCard extends Component<Props, State> {
	private fetchTimeout: number | undefined

	constructor(props?: Props | undefined, context?: any) {
		super(props, context)
		this.fetchInfo()
	}

	private fetchInfo = () => {
		sebraucApi
			.getInfo()
			.then((response) => {
				if (response.status == 200) {
					this.setState({sysinfo: response.data})
				} else {
					console.log("error fetching info", response.data)
					this.fetchTimeout = window.setTimeout(this.fetchInfo, 3000)
				}
			})
			.catch((reason) => {
				console.log("error fetching info", reason)
				this.fetchTimeout = window.setTimeout(this.fetchInfo, 3000)
			})
	}

	private renderSysinfo() {
		return (
			<div>
				<div className="card">
					<p class="top">System information</p>
					<table class="table no-bottom-border">
						<tr>
							<td>
								<Icon icon={mdiMonitor} /> Hostname
							</td>
							<td>{this.state.sysinfo.hostname}</td>
						</tr>
						<tr>
							<td>
								<Icon icon={mdiPenguin} /> Operating system
							</td>
							<td>{this.state.sysinfo.os_name}</td>
						</tr>
						<tr>
							<td>
								<Icon icon={mdiAlphaVCircleOutline} /> OS version
							</td>
							<td>{this.state.sysinfo.os_version}</td>
						</tr>
						<tr>
							<td>
								<Icon icon={mdiClockOutline} /> Uptime
							</td>
							<td>{secondsToString(this.state.sysinfo.uptime)}</td>
						</tr>
						<tr>
							<td>
								<Icon icon={mdiTagOutline} /> Compatible FW
							</td>
							<td>{this.state.sysinfo.rauc_compatible}</td>
						</tr>
						<tr>
							<td>
								<Icon icon={mdiTagMultipleOutline} /> Compatible FW
								variant
							</td>
							<td>{this.state.sysinfo.rauc_variant}</td>
						</tr>
					</table>
				</div>

				<div className="card">
					<p class="top">Rootfs slots</p>
					<div class="table-wrapper">
						<table class="table no-bottom-border">
							<thead>
								<tr>
									<th></th>
									<th>Name</th>
									<th>Device</th>
									<th>Mountpoint</th>
								</tr>
							</thead>
							<tbody>
								{Object.keys(this.state.sysinfo.rauc_rootfs).map(
									(k, i) => {
										const rfs = this.state.sysinfo.rauc_rootfs[k]
										let icon = mdiCircleOutline
										let iconColor = colors.BLUE

										if (!rfs.bootable) {
											icon = mdiCloseCircleOutline
											iconColor = colors.RED
										} else if (rfs.primary) {
											icon = mdiCheckCircleOutline
											iconColor = colors.GREEN
										}

										return (
											<tr key={i}>
												<td>
													<Icon
														icon={icon}
														color={iconColor}
													/>
												</td>
												<td>{k}</td>
												<td>{rfs.device}</td>
												<td>{rfs.mountpoint}</td>
											</tr>
										)
									}
								)}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		)
	}

	private renderLoadingAnimation() {
		return (
			<div className="card">
				<p>loading sysinfo...</p>
			</div>
		)
	}

	componentWillUnmount() {
		if (this.fetchTimeout !== undefined) {
			window.clearTimeout(this.fetchTimeout)
		}
	}

	render() {
		if (this.state.sysinfo) {
			return this.renderSysinfo()
		}
		return this.renderLoadingAnimation()
	}
}
