import {mdiCheckCircleOutline, mdiRestore} from "@mdi/js"
import {Component} from "preact"
import {sebraucApi} from "../../util/apiUrls"
import Icon from "../Icon/Icon"

export default class Reboot extends Component {
	private triggerReboot = () => {
		const res = confirm("Reboot the system?")
		if (!res) return

		sebraucApi
			.startReboot()
			.then((response) => {
				const msg = response.data.msg

				if (msg !== undefined) {
					alert(msg)
				} else {
					alert("No response")
				}
			})
			.catch((error) => {
				if (error.response) {
					const msg = error.response.data.msg

					if (msg !== undefined) {
						alert("Error: " + msg)
					} else {
						alert("Error: no response")
					}
				} else {
					alert(String(error))
				}
			})
	}

	render() {
		return (
			<div class="alert">
				<span>
					<Icon icon={mdiCheckCircleOutline} color="#148420" />
					Reboot the system to apply the update
				</span>
				<button class="iconButton" onClick={this.triggerReboot}>
					<Icon icon={mdiRestore} />
				</button>
			</div>
		)
	}
}
