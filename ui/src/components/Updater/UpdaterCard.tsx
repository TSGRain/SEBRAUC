import {Component, createRef} from "preact"
import {mdiUpload} from "@mdi/js"
import byteSize from "byte-size"
import Dropzone from "../Dropzone/Dropzone"
import ProgressCircle from "../ProgressCircle/ProgressCircle"
import Icon from "../Icon/Icon"
import "./Updater.scss"
import Alert from "./Alert"
import Reboot from "./Reboot"
import {sebraucApi} from "../../util/apiUrls"
import colors from "../../util/colors"
import WebsocketClient from "../../util/websocket"

class UploadStatus {
	uploading = false
	total = 0
	loaded = 0
	lastError = ""

	constructor(uploading: boolean, total = 0, loaded = 0, lastError = "") {
		this.uploading = uploading
		this.total = total
		this.loaded = loaded
		this.lastError = lastError
	}

	static fromProgressEvent(progressEvent: {
		loaded: number
		total: number
	}): UploadStatus {
		return new UploadStatus(true, progressEvent.total, progressEvent.loaded)
	}
}

class RaucStatus {
	installing = false
	percent = 0
	message = ""
	last_error = ""
	log = ""
}

const byteSizeOptions = {units: "metric"}

type Props = {}

type State = {
	uploadStatus: UploadStatus
	uploadFilename: string
	raucStatus: RaucStatus
	wsConnected: boolean
}

export default class UpdaterCard extends Component<Props, State> {
	private dropzoneRef = createRef<Dropzone>()
	private ws: WebsocketClient

	constructor(props?: Props | undefined, context?: any) {
		super(props, context)

		this.ws = new WebsocketClient(this.onWsStatusUpdate, this.onWsMessage)

		this.state = {
			uploadStatus: new UploadStatus(false),
			uploadFilename: "",
			raucStatus: new RaucStatus(),
			wsConnected: this.ws.api().isConnected(),
		}
	}

	private buttonClick = () => {
		if (!this.acceptUploads()) return

		this.dropzoneRef.current?.openFileDialog()
	}

	private onFilesAdded = (files: File[]) => {
		if (files.length === 0) return
		const newFile = files[0]

		this.setState({
			uploadStatus: new UploadStatus(true, newFile.size, 0),
			uploadFilename: newFile.name,
		})

		sebraucApi
			.startUpdate(newFile, {
				onUploadProgress: (progressEvent: {loaded: number; total: number}) => {
					this.setState({
						uploadStatus: UploadStatus.fromProgressEvent(progressEvent),
					})
				},
			})
			.then(() => {
				this.resetUpload()
			})
			.catch((reason: any) => {
				this.resetUpload(String(reason))
			})
	}

	private resetUpload = (lastError = "") => {
		this.setState({
			uploadStatus: new UploadStatus(false, 0, 0, lastError),
		})
		this.dropzoneRef.current?.reset()
	}

	private onWsStatusUpdate = (wsConnected: boolean) => {
		this.setState({wsConnected: wsConnected})
	}

	private onWsMessage = (evt: MessageEvent) => {
		var messages = evt.data.split("\n")
		for (var i = 0; i < messages.length; i++) {
			this.setState({
				raucStatus: Object.assign(new RaucStatus(), JSON.parse(messages[i])),
			})
		}
	}

	private acceptUploads(): boolean {
		return !this.state.uploadStatus.uploading && !this.state.raucStatus.installing
	}

	private updateCompleted(): boolean {
		return (
			!this.state.raucStatus.installing &&
			this.state.raucStatus.percent === 100 &&
			this.state.raucStatus.last_error === ""
		)
	}

	private uploadPercentage(): number {
		if (this.state.uploadStatus.uploading && this.state.uploadStatus.total > 0) {
			return Math.round(
				(this.state.uploadStatus.loaded / this.state.uploadStatus.total) * 100
			)
		}
		return 0
	}

	private circleColor(): string {
		if (this.state.raucStatus.installing) return colors.RED
		if (this.state.uploadStatus.uploading) return colors.GREEN
		return colors.BLUE
	}

	private circlePercentage(): number {
		if (this.acceptUploads()) return 0
		if (this.state.raucStatus.installing) return this.state.raucStatus.percent
		if (this.state.uploadStatus.uploading) return this.uploadPercentage()
		return 0
	}

	componentWillUnmount() {
		this.ws.destroy()
	}

	render() {
		const acceptUploads = this.acceptUploads()
		const circleColor = this.circleColor()
		const circlePercentage = this.circlePercentage()
		const updateCompleted = this.updateCompleted()

		let topText = ""
		let bottomText = ""
		let bottomText2 = ""

		if (this.state.uploadStatus.uploading) {
			topText = "Uploading"
			bottomText = this.state.uploadFilename
			bottomText2 = `${byteSize(this.state.uploadStatus.loaded, byteSizeOptions)}
			/
			${byteSize(this.state.uploadStatus.total, byteSizeOptions)}`
		} else if (this.state.raucStatus.installing) {
			topText = "Updating firmware"
			bottomText = this.state.raucStatus.message
		} else {
			topText = "Firmware update"
			bottomText = "Upload *.raucb FW package"
		}

		return (
			<div>
				<div class="card pad">
					<div>
						<p class="top">{topText}</p>
					</div>
					<Dropzone
						ref={this.dropzoneRef}
						onFilesAdded={this.onFilesAdded}
						disabled={!acceptUploads}
						accept=".raucb"
					>
						<ProgressCircle
							ready={acceptUploads}
							progress={circlePercentage}
							color={circleColor}
						>
							<button onClick={this.buttonClick} aria-label="Upload">
								<Icon icon={mdiUpload} size={50} />
							</button>
						</ProgressCircle>
					</Dropzone>
					<div>
						<p>{bottomText}</p>
						<p>{bottomText2}</p>
					</div>
				</div>
				<div>
					{this.state.wsConnected ? null : <Alert message="No connection" />}

					{this.state.uploadStatus.lastError ? (
						<Alert
							source="Upload"
							message={this.state.uploadStatus.lastError}
						/>
					) : null}
					{this.state.raucStatus.last_error ? (
						<Alert
							source="Update"
							message={this.state.raucStatus.last_error}
						/>
					) : null}
				</div>
				{updateCompleted ? <Reboot /> : null}
			</div>
		)
	}
}
