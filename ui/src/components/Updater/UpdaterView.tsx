import {mdiInformation, mdiUpload} from "@mdi/js"
import {Component} from "preact"
import Icon from "../Icon/Icon"
import SysinfoCard from "./SysinfoCard"
import UpdaterCard from "./UpdaterCard"
import "./Updater.scss"

type Props = {}

type State = {
	flipped: boolean
}

export default class UpdaterView extends Component<Props, State> {
	constructor(props?: Props | undefined, context?: any) {
		super(props, context)

		this.state = {
			flipped: false,
		}
	}

	private flipCard = () => {
		this.setState({flipped: !this.state.flipped})
	}

	render() {
		return (
			<div>
				<button
					class="iconButton button-top-right"
					onClick={this.flipCard}
					aria-label={
						this.state.flipped
							? "Switch to updater"
							: "Switch to system info"
					}
				>
					<Icon icon={this.state.flipped ? mdiUpload : mdiInformation} />
				</button>

				<div className="updater-view">
					{!this.state.flipped ? <UpdaterCard /> : <SysinfoCard />}
				</div>
			</div>
		)
	}
}
