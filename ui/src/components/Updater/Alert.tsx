import {Component} from "preact"
import {mdiTriangleOutline} from "@mdi/js"
import Icon from "../Icon/Icon"
import colors from "../../util/colors"

type Props = {
	source?: string
	message: string
}

export default class Alert extends Component<Props> {
	private stripMessage(message: string): string {
		return message.replace(/^error:/i, "").trim()
	}

	render() {
		let msg = ""
		if (this.props.source !== undefined) msg += `${this.props.source} error: `
		msg += this.stripMessage(this.props.message)

		return (
			<div class="alert">
				<span>
					<Icon icon={mdiTriangleOutline} color={colors.RED} />
					{msg}
				</span>
			</div>
		)
	}
}
