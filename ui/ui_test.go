package ui

import (
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"regexp"
	"testing"

	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestUI(t *testing.T) {
	tests := []struct {
		name     string
		path     string
		contains string
		cached   bool
	}{
		{
			name:     "index_html",
			path:     "/",
			contains: "SEBRAUC",
			cached:   false,
		},
		{
			name:     "index_html2",
			path:     "/index.html",
			contains: "SEBRAUC",
			cached:   false,
		},
		{
			name:     "index_js",
			path:     path.Join("/assets", getIndexJS()),
			contains: "SEBRAUC",
			cached:   true,
		},
	}

	router := gin.New()
	Register(router)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", tt.path, nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, http.StatusOK, w.Code)
			assert.Contains(t, w.Body.String(), tt.contains)

			ccHeader := w.Header().Get("Cache-Control")

			if tt.cached {
				assert.Equal(t, "public, max-age=604800, immutable", ccHeader)
			} else {
				assert.Equal(t, "", ccHeader)
			}
		})
	}
}

func getIndexJS() string {
	baseDir := "ui/dist/assets"
	indexExp := regexp.MustCompile(`index\.[0-9a-f]{8}\.js`)

	fixtures.CdProjectRoot()
	distDir, err := os.Open(baseDir)
	if err != nil {
		panic(err)
	}

	list, err := distDir.Readdir(-1)
	if err != nil {
		panic(err)
	}

	for _, f := range list {
		if indexExp.MatchString(f.Name()) {
			return f.Name()
		}
	}
	panic("no index.js found")
}
