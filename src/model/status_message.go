package model

// StatusMessage model
//
// StatusMessage contains the status of an operation.
//
//swagger:model StatusMessage
type StatusMessage struct {
	// Is operation successful?
	// required: true
	Success bool `json:"success"`

	// Status message text
	// required: true
	// example: Update started
	Msg string `json:"msg"`
}
