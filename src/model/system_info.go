package model

// SystemInfo model
//
// SystemInfo contains information about the running system.
//
//swagger:model SystemInfo
type SystemInfo struct {
	// Hostname of the system
	// required: true
	// example: raspberrypi3
	Hostname string `json:"hostname"`

	// Name of the os distribution
	// required: true
	// example: Poky
	OsName string `json:"os_name"`

	// Operating system version
	// required: true
	// example: 1.0.2
	OsVersion string `json:"os_version"`

	// System uptime in seconds
	// required: true
	// example: 5832
	Uptime int `json:"uptime"`

	// Compatible firmware name
	// required: true
	// example: Poky
	RaucCompatible string `json:"rauc_compatible"`

	// Compatible firmware variant
	// required: true
	// example: rpi-prod
	RaucVariant string `json:"rauc_variant"`

	// List of RAUC root filesystems
	// required: true
	RaucRootfs map[string]Rootfs `json:"rauc_rootfs"`
}

//swagger:model Rootfs
type Rootfs struct {
	// Block device
	// required: true
	// example: /dev/mmcblk0p2
	Device string `json:"device"`

	// Filesystem
	// required: true
	// example: ext4
	Type string `json:"type"`

	// Mount path (null when not mounted)
	// required: true
	// nullable: true
	// example: /
	Mountpoint *string `json:"mountpoint"`

	// Is the filesystem bootable?
	// required: true
	Bootable bool `json:"bootable"`

	// Is the filesystem booted?
	// required: true
	Booted bool `json:"booted"`

	// Is the filesystem the next boot target?
	// required: true
	Primary bool `json:"primary"`

	Bootname string `json:"-"`
}
