package model

// RaucStatus model
//
// RaucStatus contains information about the current RAUC updater status.
//
//swagger:model RaucStatus
//nolint:lll
type RaucStatus struct {
	// True if the installer is running
	// required: true
	Installing bool `json:"installing"`

	// Installation progress
	// required: true
	// minimum: 0
	// maximum: 100
	Percent int `json:"percent"`

	// Current installation step
	// required: true
	// example: Copying image to rootfs.0
	Message string `json:"message"`

	// Installation error message
	// required: true
	// example: Failed to check bundle identifier: Invalid identifier.
	LastError string `json:"last_error"`

	// Full command line output of the current installation
	// required: true
	// example: 0% Installing 0% Determining slot states 20% Determining slot states done
	Log string `json:"log"`
}
