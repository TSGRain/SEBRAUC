package model

// Error model
//
// The Error contains error relevant information.
//
//swagger:model Error
type Error struct {
	// The general error message according to HTTP specification.
	//
	// required: true
	// example: Unauthorized
	Error string `json:"error"`
	// The http error code.
	//
	// required: true
	// example: 500
	StatusCode int `json:"status_code"`
	// Concrete error message.
	//
	// required: true
	// example: already running
	Message string `json:"msg"`
}
