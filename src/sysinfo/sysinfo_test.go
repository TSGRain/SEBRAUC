package sysinfo

import (
	"path/filepath"
	"testing"

	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures"
	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures/testcmd"
	"code.thetadev.de/TSGRain/SEBRAUC/src/model"
	"github.com/stretchr/testify/assert"
)

const statusJson = `{"compatible":"TSGRain","variant":"dev","booted":"A",` +
	`"boot_primary":"rootfs.0","slots":[{"rootfs.1":{"class":"rootfs",` +
	`"device":"/dev/mmcblk0p3","type":"ext4","bootname":"B","state":"inactive",` +
	`"parent":null,"mountpoint":null,"boot_status":"good"}},{"rootfs.0":` +
	`{"class":"rootfs","device":"/dev/mmcblk0p2","type":"ext4","bootname":"A",` +
	`"state":"booted","parent":null,"mountpoint":"/","boot_status":"good"}}]}`

var mountRoot = "/"

var expectedRaucInfo = raucInfo{
	Compatible:  "TSGRain",
	Variant:     "dev",
	Booted:      "A",
	BootPrimary: "rootfs.0",
	Slots: []map[string]raucFS{
		{
			"rootfs.1": {
				Class:      "rootfs",
				Device:     "/dev/mmcblk0p3",
				Type:       "ext4",
				Bootname:   "B",
				State:      "inactive",
				Mountpoint: nil,
				BootStatus: "good",
			},
		},
		{
			"rootfs.0": {
				Class:      "rootfs",
				Device:     "/dev/mmcblk0p2",
				Type:       "ext4",
				Bootname:   "A",
				State:      "booted",
				Mountpoint: &mountRoot,
				BootStatus: "good",
			},
		},
	},
}

var expectedRootfsList = map[string]model.Rootfs{
	"rootfs.0": {
		Device:     "/dev/mmcblk0p2",
		Type:       "ext4",
		Bootname:   "A",
		Mountpoint: &mountRoot,
		Bootable:   true,
		Booted:     true,
		Primary:    true,
	},
	"rootfs.1": {
		Device:     "/dev/mmcblk0p3",
		Type:       "ext4",
		Bootname:   "B",
		Mountpoint: nil,
		Bootable:   true,
		Booted:     false,
		Primary:    false,
	},
}

func TestParseRaucInfo(t *testing.T) {
	info, err := parseRaucInfo([]byte(statusJson))
	if err != nil {
		panic(err)
	}

	assert.Equal(t, expectedRaucInfo, info)
}

func TestParseOsRelease(t *testing.T) {
	testfiles := fixtures.GetTestfilesDir()
	osReleaseFile := filepath.Join(testfiles, "os-release")

	si := New(testcmd.RaucStatus, osReleaseFile, "NAME", "VERSION",
		"/etc/hostname", "/proc/uptime")

	osRel, err := si.parseOsRelease()
	if err != nil {
		panic(err)
	}

	expected := osRelease{
		OsName:    "TSGRain distro",
		OsVersion: "0.0.1",
	}

	assert.Equal(t, expected, osRel)
}

func TestMapRootfsList(t *testing.T) {
	rootfsList := mapRootfs(expectedRaucInfo)

	assert.Equal(t, expectedRootfsList, rootfsList)
}

func TestGetFSNameFromBootname(t *testing.T) {
	rootfsList := mapRootfs(expectedRaucInfo)

	assert.Equal(t, "rootfs.0", getFSNameFromBootname(rootfsList, "A"))
	assert.Equal(t, "rootfs.1", getFSNameFromBootname(rootfsList, "B"))
	assert.Equal(t, "n/a", getFSNameFromBootname(rootfsList, "C"))
}

func TestGetSysinfo(t *testing.T) {
	si := Default(testcmd.RaucStatus)

	sysinfo, err := si.GetSysinfo()
	if err != nil {
		panic(err)
	}

	assert.Greater(t, sysinfo.Uptime, 0)
	assert.Equal(t, "TSGRain", sysinfo.RaucCompatible)
	assert.Equal(t, "dev", sysinfo.RaucVariant)
	assert.Equal(t, expectedRootfsList, sysinfo.RaucRootfs)
}
