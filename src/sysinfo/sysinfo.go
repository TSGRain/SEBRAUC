package sysinfo

import (
	"encoding/json"
	"os"
	"regexp"
	"strconv"
	"strings"

	"code.thetadev.de/TSGRain/SEBRAUC/src/model"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
)

type Sysinfo struct {
	cmdRaucStatus string
	releaseFile   string
	hostnameFile  string
	uptimeFile    string
	rexpName      *regexp.Regexp
	rexpVersion   *regexp.Regexp
}

type raucInfo struct {
	Compatible  string              `json:"compatible"`
	Variant     string              `json:"variant"`
	Booted      string              `json:"booted"`
	BootPrimary string              `json:"boot_primary"`
	Slots       []map[string]raucFS `json:"slots"`
}

type raucFS struct {
	Class      string  `json:"class"`
	Device     string  `json:"device"`
	Type       string  `json:"type"`
	Bootname   string  `json:"bootname"`
	State      string  `json:"state"`
	Mountpoint *string `json:"mountpoint"`
	BootStatus string  `json:"boot_status"`
}

type osRelease struct {
	OsName    string `json:"os_name"`
	OsVersion string `json:"os_version"`
}

var rexpUptime = regexp.MustCompile(`^\d+`)

func New(cmdRaucStatus string, releaseFile string, nameKey string, versionKey string,
	hostnameFile string, uptimeFile string,
) *Sysinfo {
	return &Sysinfo{
		cmdRaucStatus: cmdRaucStatus,
		releaseFile:   releaseFile,
		hostnameFile:  hostnameFile,
		uptimeFile:    uptimeFile,
		rexpName: regexp.MustCompile(
			`(?m)^` + regexp.QuoteMeta(nameKey) + `="(.+)"`),
		rexpVersion: regexp.MustCompile(
			`(?m)^` + regexp.QuoteMeta(versionKey) + `="(.+)"`),
	}
}

func Default(cmdRaucStatus string) *Sysinfo {
	return New(cmdRaucStatus, "/etc/os-release", "NAME", "VERSION",
		"/etc/hostname", "/proc/uptime")
}

func parseRaucInfo(raucInfoJson []byte) (raucInfo, error) {
	res := raucInfo{}
	err := json.Unmarshal(raucInfoJson, &res)
	return res, err
}

func (s *Sysinfo) parseOsRelease() (osRelease, error) {
	osReleaseTxt, err := os.ReadFile(s.releaseFile)
	if err != nil {
		return osRelease{}, err
	}

	nameMatch := s.rexpName.FindSubmatch(osReleaseTxt)
	versionMatch := s.rexpVersion.FindSubmatch(osReleaseTxt)

	name := ""
	if nameMatch != nil {
		name = string(nameMatch[1])
	}

	version := ""
	if versionMatch != nil {
		version = string(versionMatch[1])
	}

	return osRelease{
		OsName:    name,
		OsVersion: version,
	}, nil
}

func mapRootfs(rinf raucInfo) map[string]model.Rootfs {
	res := make(map[string]model.Rootfs)

	for _, slot := range rinf.Slots {
		for name, fs := range slot {
			if fs.Class == "rootfs" {
				res[name] = model.Rootfs{
					Device:     fs.Device,
					Type:       fs.Type,
					Bootname:   fs.Bootname,
					Mountpoint: fs.Mountpoint,
					Bootable:   fs.BootStatus == "good",
					Booted:     fs.State == "booted",
					Primary:    rinf.BootPrimary == name,
				}
			}
		}
	}
	return res
}

func getFSNameFromBootname(rfslist map[string]model.Rootfs, bootname string) string {
	for name, rfs := range rfslist {
		if rfs.Bootname == bootname {
			return name
		}
	}
	return "n/a"
}

func mapSysinfo(rinf raucInfo, osr osRelease, uptime int,
	hostname string,
) model.SystemInfo {
	rfslist := mapRootfs(rinf)

	return model.SystemInfo{
		Hostname:       hostname,
		OsName:         osr.OsName,
		OsVersion:      osr.OsVersion,
		Uptime:         uptime,
		RaucCompatible: rinf.Compatible,
		RaucVariant:    rinf.Variant,
		RaucRootfs:     rfslist,
	}
}

func (s *Sysinfo) getUptime() (int, error) {
	uptimeRaw, err := os.ReadFile(s.uptimeFile)
	if err != nil {
		return 0, err
	}

	uptimeChars := rexpUptime.Find(uptimeRaw)
	return strconv.Atoi(string(uptimeChars))
}

func (s *Sysinfo) getHostname() string {
	hostname, err := os.ReadFile(s.hostnameFile)
	if err != nil {
		return ""
	}
	return strings.TrimSpace(string(hostname))
}

func (s *Sysinfo) GetSysinfo() (model.SystemInfo, error) {
	cmd := util.CommandFromString(s.cmdRaucStatus)
	rinfJson, err := cmd.Output()
	if err != nil {
		return model.SystemInfo{}, err
	}

	rinf, err := parseRaucInfo(rinfJson)
	if err != nil {
		return model.SystemInfo{}, err
	}

	osinf, err := s.parseOsRelease()
	if err != nil {
		return model.SystemInfo{}, err
	}

	uptime, err := s.getUptime()
	if err != nil {
		return model.SystemInfo{}, err
	}

	hostname := s.getHostname()

	return mapSysinfo(rinf, osinf, uptime, hostname), nil
}
