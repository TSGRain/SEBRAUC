package config

import (
	"strings"

	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures/testcmd"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util/mode"
	"github.com/jinzhu/configor"
)

var (
	cfgFilePaths = []string{"sebrauc", "/etc/sebrauc/sebrauc"}
	cfgFileTypes = []string{"toml", "yaml", "yml", "json"}
)

// SEBRAUC config object
type Config struct {
	// Temporary directory
	// Update packages are stored temporarily under Tmpdir/update-<UUID>.raucb
	Tmpdir string

	// Webserver options
	Server struct {
		// IP address to accept connections from
		Address string
		// Port to listen at
		Port int `default:"80"`

		// Websocket connection settings: Ping interval/Timeout in seconds
		Websocket struct {
			Ping    int `default:"45"`
			Timeout int `default:"15"`
		}

		// Compression settings. Refer to code.thetadev.de/TSGRain/ginzip for details.
		Compression struct {
			Gzip   string
			Brotli string
		}
	}

	Authentication struct {
		Enable     bool `default:"false"`
		PasswdFile string
	}

	// Where to obtain system info from
	Sysinfo struct {
		ReleaseFile string `default:"/etc/os-release"`
		// Keys to look for in the OS release file
		NameKey      string `default:"NAME"`
		VersionKey   string `default:"VERSION"`
		HostnameFile string `default:"/etc/hostname"`
		UptimeFile   string `default:"/proc/uptime"`
	}

	// Commands to be run by SEBRAUC
	// Note that these are overriden when running in development mode
	Commands struct {
		// RAUC status command (outputs updater status in json format)
		RaucStatus string `default:"rauc status --output-format=json"`
		// RAUC install command (installs FW image passed as argument)
		RaucInstall string `default:"rauc install"`
		// System reboot command
		Reboot string `default:"shutdown -r 0"`
	}
}

func findConfigFile(pathIn string) string {
	fpath, err := util.FindFile(pathIn, cfgFilePaths, cfgFileTypes)
	if err != nil {
		if pathIn != "" {
			panic("cfg file not found: " + err.Error())
		}
		return ""
	}

	return fpath
}

func stripTrailingSlashes(pathIn string) string {
	return strings.TrimRight(pathIn, "/")
}

func loadConfig(cfgFile string) *Config {
	cfg := new(Config)
	cfgor := configor.New(&configor.Config{
		// Debug: true,
		ENVPrefix: "SEBRAUC",
	})

	err := cfgor.Load(cfg, cfgFile)
	if err != nil {
		panic(err)
	}

	// Override commands with testing options
	if mode.IsDev() {
		cfg.Commands.RaucStatus = testcmd.RaucStatus
		cfg.Commands.RaucInstall = testcmd.RaucInstall
		cfg.Commands.Reboot = testcmd.Reboot
	}

	cfg.Tmpdir = stripTrailingSlashes(cfg.Tmpdir)

	return cfg
}

// GetWithFlags returns the configuration extracted from cmdline args,
// env variables or config file.
func GetWithFlags(pathIn string, portIn int) *Config {
	cfg := loadConfig(findConfigFile(pathIn))

	// Override port with cmdline flag if set
	if portIn > 0 {
		cfg.Server.Port = portIn
	}

	return cfg
}

// Get returns the configuration extracted from env variables or config file.
func Get() *Config {
	return loadConfig(findConfigFile(""))
}

func GetDefault() *Config {
	return loadConfig("")
}
