package config

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util/mode"
	"github.com/stretchr/testify/assert"
)

func TestDefault(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	cfg := Get()

	assert.Equal(t, "", cfg.Server.Address)
	assert.Equal(t, 80, cfg.Server.Port)

	assert.Equal(t, 45, cfg.Server.Websocket.Ping)
	assert.Equal(t, 15, cfg.Server.Websocket.Timeout)

	assert.Equal(t, "", cfg.Server.Compression.Gzip)
	assert.Equal(t, "", cfg.Server.Compression.Brotli)

	assert.Equal(t, "", cfg.Tmpdir)

	assert.Equal(t, false, cfg.Authentication.Enable)
	assert.Equal(t, "", cfg.Authentication.PasswdFile)

	assert.Equal(t, "/etc/os-release", cfg.Sysinfo.ReleaseFile)
	assert.Equal(t, "NAME", cfg.Sysinfo.NameKey)
	assert.Equal(t, "VERSION", cfg.Sysinfo.VersionKey)
	assert.Equal(t, "/etc/hostname", cfg.Sysinfo.HostnameFile)
	assert.Equal(t, "/proc/uptime", cfg.Sysinfo.UptimeFile)

	assert.Equal(t, "rauc status --output-format=json", cfg.Commands.RaucStatus)
	assert.Equal(t, "rauc install", cfg.Commands.RaucInstall)
	assert.Equal(t, "shutdown -r 0", cfg.Commands.Reboot)
}

func TestConfigFile(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	cfile := filepath.Join(fixtures.GetTestfilesDir(), "sebrauc.toml")
	cfg := GetWithFlags(cfile, 0)

	assert.Equal(t, "127.0.0.1", cfg.Server.Address)
	assert.Equal(t, 8001, cfg.Server.Port)

	assert.Equal(t, 30, cfg.Server.Websocket.Ping)
	assert.Equal(t, 10, cfg.Server.Websocket.Timeout)

	assert.Equal(t, "max", cfg.Server.Compression.Gzip)
	assert.Equal(t, "false", cfg.Server.Compression.Brotli)

	assert.Equal(t, "/var/tmp", cfg.Tmpdir)

	assert.Equal(t, true, cfg.Authentication.Enable)
	assert.Equal(t, "/etc/htpasswd", cfg.Authentication.PasswdFile)

	assert.Equal(t, "/etc/release", cfg.Sysinfo.ReleaseFile)
	assert.Equal(t, "PRETTY_NAME", cfg.Sysinfo.NameKey)
	assert.Equal(t, "VER", cfg.Sysinfo.VersionKey)
	assert.Equal(t, "/etc/hn", cfg.Sysinfo.HostnameFile)
	assert.Equal(t, "/proc/up", cfg.Sysinfo.UptimeFile)

	assert.Equal(t, "myrauc status --output-format=json", cfg.Commands.RaucStatus)
	assert.Equal(t, "myrauc install", cfg.Commands.RaucInstall)
	assert.Equal(t, "reboot", cfg.Commands.Reboot)
}

func TestEnvvar(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	fixtures.ResetEnv()
	defer fixtures.ResetEnv()

	os.Setenv("SEBRAUC_TMPDIR", "/var/tmp")
	os.Setenv("SEBRAUC_SERVER_ADDRESS", "127.0.0.1")
	os.Setenv("SEBRAUC_SERVER_PORT", "8001")
	os.Setenv("SEBRAUC_SERVER_WEBSOCKET_PING", "30")
	os.Setenv("SEBRAUC_SERVER_WEBSOCKET_TIMEOUT", "10")
	os.Setenv("SEBRAUC_SERVER_COMPRESSION_GZIP", "max")
	os.Setenv("SEBRAUC_SERVER_COMPRESSION_BROTLI", "false")
	os.Setenv("SEBRAUC_AUTHENTICATION_ENABLE", "true")
	os.Setenv("SEBRAUC_AUTHENTICATION_PASSWDFILE", "/etc/htpasswd")
	os.Setenv("SEBRAUC_SYSINFO_RELEASEFILE", "/etc/release")
	os.Setenv("SEBRAUC_SYSINFO_NAMEKEY", "PRETTY_NAME")
	os.Setenv("SEBRAUC_SYSINFO_VERSIONKEY", "VER")
	os.Setenv("SEBRAUC_SYSINFO_HOSTNAMEFILE", "/etc/hn")
	os.Setenv("SEBRAUC_SYSINFO_UPTIMEFILE", "/proc/up")
	os.Setenv("SEBRAUC_COMMANDS_RAUCSTATUS", "myrauc status --output-format=json")
	os.Setenv("SEBRAUC_COMMANDS_RAUCINSTALL", "myrauc install")
	os.Setenv("SEBRAUC_COMMANDS_REBOOT", "reboot")

	cfg := Get()

	assert.Equal(t, "127.0.0.1", cfg.Server.Address)
	assert.Equal(t, 8001, cfg.Server.Port)

	assert.Equal(t, 30, cfg.Server.Websocket.Ping)
	assert.Equal(t, 10, cfg.Server.Websocket.Timeout)

	assert.Equal(t, "max", cfg.Server.Compression.Gzip)
	assert.Equal(t, "false", cfg.Server.Compression.Brotli)

	assert.Equal(t, "/var/tmp", cfg.Tmpdir)

	assert.Equal(t, true, cfg.Authentication.Enable)
	assert.Equal(t, "/etc/htpasswd", cfg.Authentication.PasswdFile)

	assert.Equal(t, "/etc/release", cfg.Sysinfo.ReleaseFile)
	assert.Equal(t, "PRETTY_NAME", cfg.Sysinfo.NameKey)
	assert.Equal(t, "VER", cfg.Sysinfo.VersionKey)
	assert.Equal(t, "/etc/hn", cfg.Sysinfo.HostnameFile)
	assert.Equal(t, "/proc/up", cfg.Sysinfo.UptimeFile)

	assert.Equal(t, "myrauc status --output-format=json", cfg.Commands.RaucStatus)
	assert.Equal(t, "myrauc install", cfg.Commands.RaucInstall)
	assert.Equal(t, "reboot", cfg.Commands.Reboot)
}

func TestDevMode(t *testing.T) {
	cfg := Get()

	//nolint:lll
	assert.Equal(t, "go run code.thetadev.de/TSGRain/SEBRAUC/src/fixtures/rauc_mock status --output-format=json", cfg.Commands.RaucStatus)
	//nolint:lll
	assert.Equal(t, "go run code.thetadev.de/TSGRain/SEBRAUC/src/fixtures/rauc_mock install", cfg.Commands.RaucInstall)
	assert.Equal(t, "touch /tmp/sebrauc_reboot_test", cfg.Commands.Reboot)
}

func TestFlags(t *testing.T) {
	cfg := GetWithFlags("", 8001)

	assert.Equal(t, 8001, cfg.Server.Port)
}

func TestStripTrailingSlashes(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{in: "/tmp", out: "/tmp"},
		{in: "/tmp/", out: "/tmp"},
		{in: "/tmp///", out: "/tmp"},
	}

	for i, tt := range tests {
		t.Run(fmt.Sprint(i), func(t *testing.T) {
			res := stripTrailingSlashes(tt.in)
			assert.Equal(t, tt.out, res)
		})
	}
}
