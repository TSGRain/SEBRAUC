package util

import (
	"errors"
	"net/http"
)

var (
	ErrAlreadyRunning   = HttpErrNew("rauc already running", http.StatusConflict)
	ErrFileDoesNotExist = errors.New("file does not exist")
	ErrPageNotFound     = HttpErrNew("page not found", http.StatusNotFound)
	ErrUnauthorized     = HttpErrNew("unauthorized", http.StatusUnauthorized)
)
