package util

import "sync"

type Counter struct {
	count uint
	mutex sync.RWMutex
}

func (c *Counter) Get() uint {
	c.mutex.RLock()
	defer c.mutex.RUnlock()

	return c.count
}

func (c *Counter) Reset() {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.count = 0
}

func (c *Counter) Increment() uint {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.count++
	return c.count
}
