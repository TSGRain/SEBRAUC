package util

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCounter(t *testing.T) {
	counter := Counter{}

	var wg sync.WaitGroup

	incrementer := func() {
		for i := 0; i < 50; i++ {
			counter.Increment()
		}
		wg.Done()
	}

	for i := 0; i < 100; i++ {
		wg.Add(1)
		go incrementer()
	}

	wg.Wait()

	assert.EqualValues(t, 5000, counter.Get())
}
