package util

type Broadcaster interface {
	Broadcast(msg []byte)
}
