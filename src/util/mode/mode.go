package mode

import "github.com/gin-gonic/gin"

const (
	// Dev for development mode.
	Dev = "dev"
	// Prod for production mode.
	Prod = "prod"
	// TestDev used for tests.
	TestDev = "testdev"
)

var currentMode = Dev

func init() {
	Set(appmode)
}

func Set(newMode string) {
	currentMode = newMode
	updateGinMode()
}

// Get returns the current mode.
func Get() string {
	return currentMode
}

// IsDev returns true if the current mode is dev mode.
func IsDev() bool {
	return Get() == Dev || Get() == TestDev
}

func updateGinMode() {
	switch Get() {
	case Dev:
		gin.SetMode(gin.DebugMode)
	case TestDev:
		gin.SetMode(gin.TestMode)
	case Prod:
		gin.SetMode(gin.ReleaseMode)
	default:
		panic("unknown mode")
	}
}
