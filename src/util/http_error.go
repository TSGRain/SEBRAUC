package util

import "errors"

type HttpError interface {
	error
	StatusCode() int
}

type httpErr struct {
	err        error
	statusCode int
}

func HttpErrWrap(e error, statusCode int) HttpError {
	return &httpErr{
		err:        e,
		statusCode: statusCode,
	}
}

func HttpErrNew(msg string, statusCode int) HttpError {
	return HttpErrWrap(errors.New(msg), statusCode)
}

func (e *httpErr) Error() string {
	if e.err == nil {
		return ""
	}
	return e.err.Error()
}

func (e *httpErr) Unwrap() error {
	return e.err
}

func (e *httpErr) StatusCode() int {
	return e.statusCode
}
