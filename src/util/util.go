package util

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

const tmpdirName = "sebrauc"

func DoesFileExist(filepath string) bool {
	_, err := os.Stat(filepath)
	return !os.IsNotExist(err)
}

func CreateDirIfNotExists(dirpath string) error {
	if _, err := os.Stat(dirpath); os.IsNotExist(err) {
		createErr := os.MkdirAll(dirpath, 0o777)
		if createErr != nil {
			return createErr
		}
	}
	return nil
}

func GetTmpdir(tmpdirPath string) string {
	tmpdir := tmpdirPath
	// Default temporary directory
	if tmpdirPath == "" {
		tmpdir = filepath.Join(os.TempDir(), tmpdirName)
	}

	err := CreateDirIfNotExists(tmpdir)
	if err != nil {
		panic(fmt.Sprintf("could not create tmpdir %s: %s", tmpdir, err))
	}

	return tmpdir
}

func RemoveTmpdir(tmpdirPath string) {
	tmpdir := GetTmpdir(tmpdirPath)
	_ = os.RemoveAll(tmpdir)
}

func CommandFromString(cmdString string) *exec.Cmd {
	parts := strings.Split(cmdString, " ")
	if len(parts) == 0 {
		return exec.Command("")
	}

	//nolint:gosec
	return exec.Command(parts[0], parts[1:]...)
}

func Reboot(rebootCmd string, t time.Duration) {
	time.Sleep(t)
	cmd := CommandFromString(rebootCmd)
	_ = cmd.Run()
}

func FindFile(explicitPath string, locations, endings []string) (string, error) {
	if explicitPath != "" {
		if !DoesFileExist(explicitPath) {
			return "", fmt.Errorf("file %s not found", explicitPath)
		}
		return explicitPath, nil
	}

	notFound := []string{}

	for _, f := range locations {
		if endings != nil {
			for _, t := range endings {
				fpath := f + "." + t

				if DoesFileExist(fpath) {
					return fpath, nil
				} else {
					notFound = append(notFound, fpath)
				}
			}
		} else {
			if DoesFileExist(f) {
				return f, nil
			} else {
				notFound = append(notFound, f)
			}
		}
	}
	return "", fmt.Errorf("none of the following files found: %s",
		strings.Join(notFound, "; "))
}
