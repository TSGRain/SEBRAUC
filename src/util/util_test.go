package util

import (
	"errors"
	"os"
	"path/filepath"
	"testing"

	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures"
	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures/testcmd"
	"github.com/stretchr/testify/assert"
)

func TestDoesFileExist(t *testing.T) {
	fixtures.CdProjectRoot()

	type args struct {
		filepath string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "exist",
			args: args{"go.mod"},
			want: true,
		},
		{
			name: "not_exist",
			args: args{"banana.mod"},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DoesFileExist(tt.args.filepath); got != tt.want {
				t.Errorf("DoesFileExist() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTmpdir(t *testing.T) {
	tests := []struct {
		name string
		path string
	}{
		{
			name: "default",
			path: "",
		},
		{
			name: "custom",
			path: filepath.Join(os.TempDir(), "customTmpdir"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			td := GetTmpdir(tt.path)
			assert.DirExists(t, td)

			tfile := filepath.Join(td, "test.txt")
			f, err := os.Create(tfile)
			if err != nil {
				panic(err)
			}

			_, err = f.WriteString("Hello")
			if err != nil {
				panic(err)
			}
			err = f.Close()
			if err != nil {
				panic(err)
			}

			assert.FileExists(t, tfile)

			RemoveTmpdir(tt.path)
			assert.NoDirExists(t, td)
		})
	}
}

func TestCommandFromString(t *testing.T) {
	tests := []struct {
		name      string
		cmdString string
		expCmd    []string
	}{
		{
			name:      "single",
			cmdString: "ls",
			expCmd:    []string{"ls"},
		},
		{
			name:      "1arg",
			cmdString: "echo Hello",
			expCmd:    []string{"echo", "Hello"},
		},
		{
			name:      "3args",
			cmdString: "echo Hello its me",
			expCmd:    []string{"echo", "Hello", "its", "me"},
		},
		{
			name:      "empty",
			cmdString: "",
			expCmd:    []string{""},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := CommandFromString(tt.cmdString)
			assert.Equal(t, tt.expCmd, cmd.Args)
		})
	}
}

func TestReboot(t *testing.T) {
	testfile := "/tmp/sebrauc_reboot_test"
	_ = os.Remove(testfile)

	Reboot(testcmd.Reboot, 0)

	assert.FileExists(t, testfile)
}

func TestFindFile(t *testing.T) {
	testfiles := fixtures.GetTestfilesDir()

	//nolint:lll
	tests := []struct {
		name         string
		explicitPath string
		locations    []string
		endings      []string
		expect       string
		expectErr    error
	}{
		{
			name:         "locations",
			explicitPath: "",
			locations:    []string{filepath.Join(testfiles, "sebrauc")},
			endings:      []string{"yml", "toml"},
			expect:       filepath.Join(testfiles, "sebrauc.toml"),
			expectErr:    nil,
		},
		{
			name:         "locations_nf",
			explicitPath: "",
			locations:    []string{filepath.Join(testfiles, "banana")},
			endings:      []string{"yml", "toml"},
			expect:       "",
			expectErr:    errors.New("none of the following files found: src/fixtures/testfiles/banana.yml; src/fixtures/testfiles/banana.toml"),
		},
		{
			name:         "no_endings",
			explicitPath: "",
			locations:    []string{filepath.Join(testfiles, "banana"), filepath.Join(testfiles, "os-release")},
			endings:      nil,
			expect:       filepath.Join(testfiles, "os-release"),
			expectErr:    nil,
		},
		{
			name:         "no_endings_nf",
			explicitPath: "",
			locations:    []string{filepath.Join(testfiles, "banana"), filepath.Join(testfiles, "apple")},
			endings:      nil,
			expect:       "",
			expectErr:    errors.New("none of the following files found: src/fixtures/testfiles/banana; src/fixtures/testfiles/apple"),
		},
		{
			name:         "explicit",
			explicitPath: filepath.Join(testfiles, "sebrauc.toml"),
			locations:    []string{filepath.Join(testfiles, "banana")},
			endings:      []string{"yml", "toml"},
			expect:       filepath.Join(testfiles, "sebrauc.toml"),
			expectErr:    nil,
		},
		{
			name:         "explicit_nf",
			explicitPath: filepath.Join(testfiles, "banana.toml"),
			locations:    []string{filepath.Join(testfiles, "banana")},
			endings:      []string{"yml", "toml"},
			expect:       "",
			expectErr:    errors.New("file src/fixtures/testfiles/banana.toml not found"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fpath, err := FindFile(tt.explicitPath, tt.locations, tt.endings)
			assert.Equal(t, tt.expectErr, err)
			assert.Equal(t, tt.expect, fpath)
		})
	}
}
