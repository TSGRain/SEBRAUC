package swagger

import (
	_ "embed"

	"code.thetadev.de/TSGRain/SEBRAUC/src/server/middleware"
	"github.com/gin-gonic/gin"
)

//go:embed swagger.html
var swaggerHtml []byte

//go:embed swagger.yaml
var swaggerYaml []byte

func Register(r gin.IRouter) {
	swg := r.Group("/api/swagger", middleware.Cache)

	swg.GET("/", func(c *gin.Context) {
		c.Data(200, "text/html", swaggerHtml)
	})
	swg.GET("/swagger.yaml", func(c *gin.Context) {
		c.Data(200, "text/yaml", swaggerYaml)
	})
}
