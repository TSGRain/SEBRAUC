package swagger

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestSwagger(t *testing.T) {
	router := gin.New()
	Register(router)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/swagger/", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, swaggerHtml, w.Body.Bytes())
	assert.NotEmpty(t, w.Header().Get("Cache-Control"))

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/api/swagger/swagger.yaml", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, swaggerYaml, w.Body.Bytes())
	assert.NotEmpty(t, w.Header().Get("Cache-Control"))
}

func TestSwaggerData(t *testing.T) {
	assert.True(t, bytes.Contains(swaggerHtml,
		[]byte("https://cdn.jsdelivr.net/npm/redoc/bundles/redoc.standalone.js")),
		"HTML data missing",
	)

	assert.True(t, bytes.Contains(swaggerYaml,
		[]byte("REST API for the SEBRAUC firmware updater")),
		"YAML data missing",
	)
}
