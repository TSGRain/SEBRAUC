package stream

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"code.thetadev.de/TSGRain/SEBRAUC/src/util/mode"
	"github.com/fortytw2/leaktest"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
)

func TestFailureOnNormalHttpRequest(t *testing.T) {
	mode.Set(mode.TestDev)
	defer mode.Set(mode.Dev)

	defer leaktest.Check(t)()

	server, api := bootTestServer()
	defer server.Close()
	defer api.Close()

	resp, err := http.Get(server.URL)
	assert.Nil(t, err)
	assert.Equal(t, 400, resp.StatusCode)
	resp.Body.Close()
}

func TestWriteMessageFails(t *testing.T) {
	mode.Set(mode.TestDev)
	defer mode.Set(mode.Dev)

	oldWrite := writeBytes
	// try emulate an write error, mostly this should kill the ReadMessage
	// goroutine first but you'll never know.
	writeBytes = func(conn *websocket.Conn, data []byte) error {
		return errors.New("asd")
	}
	defer func() {
		writeBytes = oldWrite
	}()
	defer leaktest.Check(t)()

	server, api := bootTestServer()
	defer server.Close()
	defer api.Close()

	wsURL := wsURL(server.URL)
	user := testClient(t, wsURL)

	// the server may take some time to register the client
	time.Sleep(100 * time.Millisecond)
	client := getClient(api, 1)
	assert.NotNil(t, client)

	api.Notify(1, []byte("HI"))
	user.expectNoMessage()
}

func TestWritePingFails(t *testing.T) {
	mode.Set(mode.TestDev)
	defer mode.Set(mode.Dev)

	oldPing := ping
	// try emulate an write error, mostly this should kill the ReadMessage
	// gorouting first but you'll never know.
	ping = func(conn *websocket.Conn) error {
		return errors.New("asd")
	}
	defer func() {
		ping = oldPing
	}()

	defer leaktest.CheckTimeout(t, 10*time.Second)()

	server, api := bootTestServer()
	defer api.Close()
	defer server.Close()

	wsURL := wsURL(server.URL)
	user := testClient(t, wsURL)
	defer user.conn.Close()

	// the server may take some time to register the client
	time.Sleep(100 * time.Millisecond)
	client := getClient(api, 1)

	assert.NotNil(t, client)

	time.Sleep(api.pingPeriod) // waiting for ping

	api.Notify(1, []byte("HI"))
	user.expectNoMessage()
}

func TestPing(t *testing.T) {
	mode.Set(mode.TestDev)
	defer mode.Set(mode.Dev)

	server, api := bootTestServer()
	defer server.Close()
	defer api.Close()

	wsURL := wsURL(server.URL)

	user := createClient(t, wsURL)
	defer user.conn.Close()

	ping := make(chan bool)
	oldPingHandler := user.conn.PingHandler()
	user.conn.SetPingHandler(func(appData string) error {
		err := oldPingHandler(appData)
		ping <- true
		return err
	})

	startReading(user)

	expectNoMessage(user)

	select {
	case <-time.After(2 * time.Second):
		assert.Fail(t, "Expected ping but there was one :(")
	case <-ping:
		// expected
	}

	expectNoMessage(user)
	api.Notify(1, []byte("HI"))
	user.expectMessage([]byte("HI"))
}

func TestCloseClientOnNotReading(t *testing.T) {
	mode.Set(mode.TestDev)
	defer mode.Set(mode.Dev)

	server, api := bootTestServer()
	defer server.Close()
	defer api.Close()

	wsURL := wsURL(server.URL)

	ws, resp, err := websocket.DefaultDialer.Dial(wsURL, nil)
	assert.Nil(t, err)
	resp.Body.Close()
	defer ws.Close()

	// the server may take some time to register the client
	time.Sleep(100 * time.Millisecond)
	assert.NotNil(t, getClient(api, 1))

	time.Sleep(api.pingPeriod + api.pongTimeout)

	assert.Nil(t, getClient(api, 1))
}

func TestMessageDirectlyAfterConnect(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	defer leaktest.Check(t)()
	server, api := bootTestServer()
	defer server.Close()
	defer api.Close()

	wsURL := wsURL(server.URL)

	user := testClient(t, wsURL)
	defer user.conn.Close()
	// the server may take some time to register the client
	time.Sleep(100 * time.Millisecond)
	api.Notify(1, []byte("msg"))
	user.expectMessage([]byte("msg"))
}

func TestDeleteClientShouldCloseConnection(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	defer leaktest.Check(t)()
	server, api := bootTestServer()
	defer server.Close()
	defer api.Close()

	wsURL := wsURL(server.URL)

	user := testClient(t, wsURL)
	defer user.conn.Close()
	// the server may take some time to register the client
	time.Sleep(100 * time.Millisecond)
	api.Notify(1, []byte("HI"))
	user.expectMessage([]byte("HI"))

	assert.Nil(t, api.NotifyDeletedClient(1))

	api.Notify(1, []byte("HI"))
	user.expectNoMessage()
}

func TestNotify(t *testing.T) {
	mode.Set(mode.TestDev)
	defer mode.Set(mode.Dev)

	defer leaktest.Check(t)()
	server, api := bootTestServer()
	defer server.Close()

	wsURL := wsURL(server.URL)

	client1 := testClient(t, wsURL)
	defer client1.conn.Close()

	client2 := testClient(t, wsURL)
	defer client2.conn.Close()

	client3 := testClient(t, wsURL)
	defer client3.conn.Close()

	// the server may take some time to register the client
	time.Sleep(100 * time.Millisecond)

	api.Notify(1, []byte("msg"))
	expectMessage([]byte("msg"), client1)
	expectNoMessage(client2)
	expectNoMessage(client3)

	assert.Nil(t, api.NotifyDeletedClient(1))

	api.Notify(1, []byte("msg"))
	expectNoMessage(client1)
	expectNoMessage(client2)
	expectNoMessage(client3)

	api.Notify(2, []byte("msg"))
	expectNoMessage(client1)
	expectMessage([]byte("msg"), client2)
	expectNoMessage(client3)

	api.Notify(3, []byte("msg"))
	expectNoMessage(client1)
	expectNoMessage(client2)
	expectMessage([]byte("msg"), client3)

	api.Close()
}

func TestBroadcast(t *testing.T) {
	defer leaktest.Check(t)()
	server, api := bootTestServer()
	defer server.Close()

	wsURL := wsURL(server.URL)

	client1 := testClient(t, wsURL)
	defer client1.conn.Close()

	client2 := testClient(t, wsURL)
	defer client2.conn.Close()

	client3 := testClient(t, wsURL)
	defer client3.conn.Close()

	// the server may take some time to register the client
	time.Sleep(100 * time.Millisecond)

	testMsg1 := []byte("hello1")
	api.Broadcast(testMsg1)
	expectMessage(testMsg1, client1, client2, client3)

	assert.Nil(t, api.NotifyDeletedClient(1))

	testMsg2 := []byte("hello2")
	api.Broadcast(testMsg2)
	expectNoMessage(client1)
	expectMessage(testMsg2, client2, client3)
}

func TestLastBroadcast(t *testing.T) {
	defer leaktest.Check(t)()
	server, api := bootTestServer()
	defer server.Close()

	wsURL := wsURL(server.URL)

	testMsg1 := []byte("hello1")
	api.Broadcast(testMsg1)

	client1 := testClient(t, wsURL)
	defer client1.conn.Close()

	client2 := testClient(t, wsURL)
	defer client2.conn.Close()

	// the server may take some time to register the client
	time.Sleep(100 * time.Millisecond)

	expectMessage(testMsg1, client1, client2)
}

func Test_sameOrigin_returnsTrue(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	req := httptest.NewRequest("GET", "http://example.com/stream", nil)
	req.Header.Set("Origin", "http://example.com")
	actual := isAllowedOrigin(req, nil)
	assert.True(t, actual)
}

func Test_sameOrigin_returnsTrue_withCustomPort(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)
	req := httptest.NewRequest("GET", "http://example.com:8080/stream", nil)
	req.Header.Set("Origin", "http://example.com:8080")
	actual := isAllowedOrigin(req, nil)
	assert.True(t, actual)
}

func Test_isAllowedOrigin_withoutAllowedOrigins_failsWhenNotSameOrigin(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	req := httptest.NewRequest("GET", "http://example.com/stream", nil)
	req.Header.Set("Origin", "http://gorify.example.com")
	actual := isAllowedOrigin(req, nil)
	assert.False(t, actual)
}

func Test_isAllowedOriginMatching(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	compiledAllowedOrigins := compileAllowedWebSocketOrigins(
		[]string{"go.{4}\\.example\\.com", "go\\.example\\.com"},
	)

	req := httptest.NewRequest("GET", "http://example.me/stream", nil)
	req.Header.Set("Origin", "http://gorify.example.com")
	assert.True(t, isAllowedOrigin(req, compiledAllowedOrigins))

	req.Header.Set("Origin", "http://go.example.com")
	assert.True(t, isAllowedOrigin(req, compiledAllowedOrigins))

	req.Header.Set("Origin", "http://hello.example.com")
	assert.False(t, isAllowedOrigin(req, compiledAllowedOrigins))
}

func Test_emptyOrigin_returnsTrue(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	req := httptest.NewRequest("GET", "http://example.com/stream", nil)
	actual := isAllowedOrigin(req, nil)
	assert.True(t, actual)
}

func Test_otherOrigin_returnsFalse(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	req := httptest.NewRequest("GET", "http://example.com/stream", nil)
	req.Header.Set("Origin", "http://otherexample.de")
	actual := isAllowedOrigin(req, nil)
	assert.False(t, actual)
}

func Test_invalidOrigin_returnsFalse(t *testing.T) {
	mode.Set(mode.Prod)
	defer mode.Set(mode.Dev)

	req := httptest.NewRequest("GET", "http://example.com/stream", nil)
	req.Header.Set("Origin", "http\\://otherexample.de")
	actual := isAllowedOrigin(req, nil)
	assert.False(t, actual)
}

func Test_compileAllowedWebSocketOrigins(t *testing.T) {
	assert.Equal(t, 0, len(compileAllowedWebSocketOrigins([]string{})))
	assert.Equal(t, 3, len(compileAllowedWebSocketOrigins([]string{"^.*$", "", "abc"})))
}

func getClient(api *API, user uint) *client {
	api.lock.RLock()
	defer api.lock.RUnlock()

	return api.clients[user]
}

func testClient(t *testing.T, url string) *testingClient {
	client := createClient(t, url)
	startReading(client)
	return client
}

func startReading(client *testingClient) {
	go func() {
		for {
			_, payload, err := client.conn.ReadMessage()
			if err != nil {
				return
			}

			client.readMessage <- payload
		}
	}()
}

func createClient(t *testing.T, url string) *testingClient {
	ws, resp, err := websocket.DefaultDialer.Dial(url, nil)
	assert.Nil(t, err)
	resp.Body.Close()

	readMessages := make(chan []byte)

	return &testingClient{conn: ws, readMessage: readMessages, t: t}
}

type testingClient struct {
	conn        *websocket.Conn
	readMessage chan []byte
	t           *testing.T
}

func (c *testingClient) expectMessage(expected []byte) {
	select {
	case <-time.After(50 * time.Millisecond):
		assert.Fail(c.t, "Expected message but none was send :(")
	case actual := <-c.readMessage:
		assert.Equal(c.t, expected, actual)
	}
}

func expectMessage(expected []byte, clients ...*testingClient) {
	for _, client := range clients {
		client.expectMessage(expected)
	}
}

func expectNoMessage(clients ...*testingClient) {
	for _, client := range clients {
		client.expectNoMessage()
	}
}

func (c *testingClient) expectNoMessage() {
	select {
	case <-time.After(50 * time.Millisecond):
		// no message == as expected
	case msg := <-c.readMessage:
		assert.Fail(c.t, "Expected NO message but there was one :(", fmt.Sprint(msg))
	}
}

func bootTestServer() (*httptest.Server, *API) {
	r := gin.New()
	// ping every 500 ms, and the client has 500 ms to respond
	api := New(500*time.Millisecond, 500*time.Millisecond, []string{})

	r.GET("/", api.Handle)
	server := httptest.NewServer(r)
	return server, api
}

func wsURL(httpURL string) string {
	return "ws" + strings.TrimPrefix(httpURL, "http")
}
