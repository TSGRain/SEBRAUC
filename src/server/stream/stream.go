package stream

import (
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util/mode"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// The API provides a handler for a WebSocket stream API.
type API struct {
	clients       map[uint]*client
	lock          sync.RWMutex
	pingPeriod    time.Duration
	pongTimeout   time.Duration
	upgrader      *websocket.Upgrader
	counter       *util.Counter
	lastBroadcast []byte
}

// New creates a new instance of API.
// pingPeriod: is the interval, in which is server sends the a ping to the client.
// pongTimeout: is the duration after the connection will be terminated,
// when the client does not respond with the pong command.
func New(pingPeriod, pongTimeout time.Duration, allowedWebSocketOrigins []string) *API {
	return &API{
		clients:     make(map[uint]*client),
		pingPeriod:  pingPeriod,
		pongTimeout: pingPeriod + pongTimeout,
		upgrader:    newUpgrader(allowedWebSocketOrigins),
		counter:     &util.Counter{},
	}
}

// NotifyDeletedUser closes existing connections for the given user.
func (a *API) NotifyDeletedClient(userID uint) error {
	a.lock.Lock()
	defer a.lock.Unlock()
	if client, ok := a.clients[userID]; ok {
		client.Close()
		delete(a.clients, userID)
	}
	return nil
}

// Notify notifies the clients with the given userID that a new messages was created.
func (a *API) Notify(userID uint, msg []byte) {
	a.lock.RLock()
	defer a.lock.RUnlock()
	if client, ok := a.clients[userID]; ok {
		client.write <- msg
	}
}

func (a *API) Broadcast(msg []byte) {
	a.lock.RLock()
	defer a.lock.RUnlock()
	for _, client := range a.clients {
		client.write <- msg
	}
	a.lastBroadcast = msg
}

func (a *API) remove(remove *client) {
	a.lock.Lock()
	defer a.lock.Unlock()
	delete(a.clients, remove.id)
}

func (a *API) register(client *client) {
	a.lock.Lock()
	defer a.lock.Unlock()
	a.clients[client.id] = client

	// Send new clients the last broadcast so they get the current state
	if a.lastBroadcast != nil {
		client.write <- a.lastBroadcast
	}
}

func (a *API) Handle(ctx *gin.Context) {
	conn, err := a.upgrader.Upgrade(ctx.Writer, ctx.Request, nil)
	if err != nil {
		ctx.Error(err)
		return
	}

	client := newClient(conn, a.counter.Increment(), a.remove)
	a.register(client)
	go client.startReading(a.pongTimeout)
	go client.startWriteHandler(a.pingPeriod)
}

// Close closes all client connections and stops answering new connections.
func (a *API) Close() {
	a.lock.Lock()
	defer a.lock.Unlock()

	for _, client := range a.clients {
		client.Close()
	}
	for k := range a.clients {
		delete(a.clients, k)
	}
}

func isAllowedOrigin(r *http.Request, allowedOrigins []*regexp.Regexp) bool {
	origin := r.Header.Get("origin")
	if origin == "" {
		return true
	}

	u, err := url.Parse(origin)
	if err != nil {
		return false
	}

	if strings.EqualFold(u.Host, r.Host) {
		return true
	}

	for _, allowedOrigin := range allowedOrigins {
		if allowedOrigin.Match([]byte(strings.ToLower(u.Hostname()))) {
			return true
		}
	}

	return false
}

func newUpgrader(allowedWebSocketOrigins []string) *websocket.Upgrader {
	compiledAllowedOrigins := compileAllowedWebSocketOrigins(allowedWebSocketOrigins)

	return &websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			if mode.IsDev() {
				return true
			}
			return isAllowedOrigin(r, compiledAllowedOrigins)
		},
	}
}

func compileAllowedWebSocketOrigins(allowedOrigins []string) []*regexp.Regexp {
	var compiledAllowedOrigins []*regexp.Regexp
	for _, origin := range allowedOrigins {
		compiledAllowedOrigins = append(compiledAllowedOrigins,
			regexp.MustCompile(origin))
	}

	return compiledAllowedOrigins
}
