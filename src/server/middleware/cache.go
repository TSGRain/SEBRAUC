package middleware

import "github.com/gin-gonic/gin"

func Cache(c *gin.Context) {
	c.Writer.Header().Set("Cache-Control", "public, max-age=604800, immutable")
}
