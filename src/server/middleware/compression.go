package middleware

import (
	"code.thetadev.de/TSGRain/ginzip"
	"github.com/gin-gonic/gin"
)

func Compression(gzip, brotli string) gin.HandlerFunc {
	opts := ginzip.DefaultOptions()
	opts.GzipLevel = gzip
	opts.BrotliLevel = brotli

	return ginzip.New(opts)
}
