package middleware

import (
	"net/http"

	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
	"github.com/gin-gonic/gin"
	"github.com/tg123/go-htpasswd"
)

var pwdFilePaths = []string{"htpasswd", "/etc/sebrauc/htpasswd"}

// Authentication requires HTTP basic auth or an active session
func Authentication(pwdFile string) gin.HandlerFunc {
	fpath, err := util.FindFile(pwdFile, pwdFilePaths, nil)
	if err != nil {
		panic("passwd file not found: " + err.Error())
	}

	myauth, err := htpasswd.New(fpath, htpasswd.DefaultSystems, nil)
	if err != nil {
		panic(err)
	}

	return func(c *gin.Context) {
		if user, pass, ok := c.Request.BasicAuth(); ok {
			if myauth.Match(user, pass) {
				c.Set(gin.AuthUserKey, user)
				return
			}
		}

		c.Header("WWW-Authenticate", "Basic realm=\"Authorization Required\"")
		c.AbortWithStatus(http.StatusUnauthorized)
	}
}
