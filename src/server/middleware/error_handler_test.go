package middleware

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestErrorHandler(t *testing.T) {
	tests := []struct {
		name           string
		controller     gin.HandlerFunc
		isApi          bool
		expectResponse string
		expectStatus   int
	}{
		{
			name:           "error",
			controller:     controllerError,
			isApi:          false,
			expectResponse: "400 Bad Request: error test",
			expectStatus:   http.StatusBadRequest,
		},
		{
			name:       "error_api",
			controller: controllerError,
			isApi:      true,
			//nolint:lll
			expectResponse: `{"error":"Bad Request","status_code":400,"msg":"error test"}`,
			expectStatus:   http.StatusBadRequest,
		},
		{
			name:           "generic_error",
			controller:     controllerErrorGeneric,
			isApi:          false,
			expectResponse: "500 Internal Server Error: generic error",
			expectStatus:   http.StatusInternalServerError,
		},
		{
			name:       "generic_error_api",
			controller: controllerErrorGeneric,
			isApi:      true,
			//nolint:lll
			expectResponse: `{"error":"Internal Server Error","status_code":500,"msg":"generic error"}`,
			expectStatus:   http.StatusInternalServerError,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := gin.New()
			router.Use(ErrorHandler(tt.isApi))
			router.GET("/", tt.controller)

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/", nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.expectStatus, w.Code)
			assert.Equal(t, tt.expectResponse, w.Body.String())
		})
	}
}

func TestPanicHandler(t *testing.T) {
	tests := []struct {
		name           string
		controller     gin.HandlerFunc
		isApi          bool
		expectResponse string
		expectStatus   int
	}{
		{
			name:           "panic",
			controller:     controllerPanic,
			isApi:          false,
			expectResponse: "500 Internal Server Error: [PANIC] panic message",
			expectStatus:   http.StatusInternalServerError,
		},
		{
			name:       "panic_api",
			controller: controllerPanic,
			isApi:      true,
			//nolint:lll
			expectResponse: `{"error":"Internal Server Error","status_code":500,"msg":"[PANIC] panic message"}`,
			expectStatus:   http.StatusInternalServerError,
		},
		{
			name:           "panic_w_error",
			controller:     controllerPanicErr,
			isApi:          false,
			expectResponse: "500 Internal Server Error: [PANIC] panic message in error",
			expectStatus:   http.StatusInternalServerError,
		},
		{
			name:       "panic_w_error_api",
			controller: controllerPanicErr,
			isApi:      true,
			//nolint:lll
			expectResponse: `{"error":"Internal Server Error","status_code":500,"msg":"[PANIC] panic message in error"}`,
			expectStatus:   http.StatusInternalServerError,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := gin.New()
			router.Use(PanicHandler(tt.isApi))
			router.GET("/", tt.controller)

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/", nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.expectStatus, w.Code)
			assert.Equal(t, tt.expectResponse, w.Body.String())
		})
	}
}

func controllerError(c *gin.Context) {
	c.Error(util.HttpErrNew("error test", http.StatusBadRequest))
}

func controllerErrorGeneric(c *gin.Context) {
	c.Error(errors.New("generic error"))
}

func controllerPanic(c *gin.Context) {
	panic("panic message")
}

func controllerPanicErr(c *gin.Context) {
	panic(errors.New("panic message in error"))
}
