package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestCache(t *testing.T) {
	router := gin.New()
	router.Use(Cache)
	router.GET("/", func(c *gin.Context) { c.String(http.StatusOK, "HelloWorld") })

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "HelloWorld", w.Body.String())
	assert.Equal(t, "public, max-age=604800, immutable",
		w.Header().Get("Cache-Control"))
}
