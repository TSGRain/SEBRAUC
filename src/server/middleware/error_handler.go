package middleware

import (
	"errors"
	"fmt"
	"net/http"

	"code.thetadev.de/TSGRain/SEBRAUC/src/model"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
	nice "github.com/ekyoung/gin-nice-recovery"
	"github.com/gin-gonic/gin"
)

// ErrorHandler creates a gin middleware for handling errors.
func ErrorHandler(isApi bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		if len(c.Errors) > 0 {
			for _, e := range c.Errors {
				writeError(c, e.Err, isApi)
			}
		}
	}
}

func PanicHandler(isApi bool) gin.HandlerFunc {
	return nice.Recovery(func(c *gin.Context, err interface{}) {
		writeError(c, fmt.Errorf("[PANIC] %s", err), isApi)
	})
}

func writeError(c *gin.Context, err error, isApi bool) {
	status := http.StatusInternalServerError

	var httpErr util.HttpError
	if errors.As(err, &httpErr) {
		status = httpErr.StatusCode()
	}

	// only write error message if there is no content
	if c.Writer.Size() != -1 {
		c.Status(status)
		return
	}

	if isApi {
		// Machine-readable JSON error message
		c.JSON(status, &model.Error{
			Error:      http.StatusText(status),
			StatusCode: status,
			Message:    err.Error(),
		})
	} else {
		// Human-readable error message
		c.String(status, "%d %s: %s", status, http.StatusText(status), err.Error())
	}
}
