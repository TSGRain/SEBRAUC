package middleware

import (
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"

	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestAuthentication(t *testing.T) {
	testfiles := fixtures.GetTestfilesDir()
	pwdfile := filepath.Join(testfiles, "htpasswd")

	router := gin.New()
	router.Use(Authentication(pwdfile))
	router.GET("/", func(c *gin.Context) { c.String(http.StatusOK, "HelloWorld") })

	tests := []struct {
		name string
	}{
		{name: "plain"},
		{name: "md5"},
		{name: "bcrypt"},
	}

	for _, tt := range tests {
		t.Run(tt.name+"_ok", func(t *testing.T) {
			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/", nil)
			req.SetBasicAuth(tt.name, "1234")
			router.ServeHTTP(w, req)

			assert.Equal(t, http.StatusOK, w.Code)
			assert.Equal(t, "HelloWorld", w.Body.String())
			assert.Empty(t, w.Header().Get("WWW-Authenticate"))
		})
	}

	t.Run("fail", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/", nil)
		req.SetBasicAuth("plain", "asdf")
		router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusUnauthorized, w.Code)
		assert.Empty(t, w.Body.String())
		assert.Equal(t, "Basic realm=\"Authorization Required\"",
			w.Header().Get("WWW-Authenticate"))
	})
}
