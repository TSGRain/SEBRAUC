package server

import (
	"bytes"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"regexp"
	"testing"
	"time"

	"code.thetadev.de/TSGRain/SEBRAUC/src/config"
	"code.thetadev.de/TSGRain/SEBRAUC/src/fixtures"
	"code.thetadev.de/TSGRain/SEBRAUC/src/model"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

type testServer struct {
	srv    *SEBRAUCServer
	router *gin.Engine
}

func newTestServer() *testServer {
	return newTestServerCfg(config.GetDefault())
}

func newTestServerCfg(cfg *config.Config) *testServer {
	sebraucServer := NewServer(cfg)
	router := sebraucServer.getRouter()

	return &testServer{
		srv:    sebraucServer,
		router: router,
	}
}

func (srv *testServer) testRequest(t assert.TestingT, method string, url string,
	body io.Reader, contentType string,
) *httptest.ResponseRecorder {
	req, err := http.NewRequest(method, url, body)
	req.Header.Set("Content-Type", contentType)
	assert.Nil(t, err)

	w := httptest.NewRecorder()
	srv.router.ServeHTTP(w, req)

	return w
}

func TestUpdate(t *testing.T) {
	fixtures.ResetEnv()
	defer fixtures.ResetEnv()
	util.RemoveTmpdir("")

	os.Setenv("RAUC_MOCK_TEST", "1")

	srv := newTestServer()

	updateContent := []byte("mock update file")

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("updateFile", "update.raucb")
	assert.Nil(t, err)
	_, err = part.Write(updateContent)
	assert.Nil(t, err)
	err = writer.Close()
	assert.Nil(t, err)

	w := srv.testRequest(t, "POST", "/api/update", body, writer.FormDataContentType())

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	assert.Equal(t,
		`{"success":true,"msg":"Update started"}`,
		w.Body.String(),
	)

	// Find update file
	tmpdir := util.GetTmpdir("")
	//nolint:lll
	updateFileExp := regexp.MustCompile(
		`update_[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\.raucb`)
	updateFile := ""

	tmpdirObj, err := os.Open(tmpdir)
	assert.Nil(t, err)
	list, err := tmpdirObj.ReadDir(-1)
	assert.Nil(t, err)

	for _, f := range list {
		if updateFileExp.MatchString(f.Name()) {
			updateFile = filepath.Join(tmpdir, f.Name())
			break
		}
	}
	assert.NotEmpty(t, updateFile, "update file not found")

	// Check update file
	content, err := os.ReadFile(updateFile)
	assert.Nil(t, err)
	assert.Equal(t, updateContent, content)

	// Wait for update to complete
	time.Sleep(1000 * time.Millisecond)

	// Update file should be removed when update is completed
	assert.NoFileExists(t, updateFile)

	// Get final status
	w = srv.testRequest(t, "GET", "/api/status", nil, "")

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	//nolint:lll
	assert.Equal(t,
		`{"installing":false,"percent":100,"message":"Installing done.","last_error":"","log":"0% Installing\n0% Determining slot states\n20% Determining slot states done.\n20% Checking bundle\n20% Verifying signature\n40% Verifying signature done.\n40% Checking bundle done.\n40% Checking manifest contents\n60% Checking manifest contents done.\n60% Determining target install group\n80% Determining target install group done.\n80% Updating slots\n80% Checking slot rootfs.0\n90% Checking slot rootfs.0 done.\n90% Copying image to rootfs.0\n100% Copying image to rootfs.0 done.\n100% Updating slots done.\n100% Installing done.\nInstalling `+"`/app/tsgrain-update-raspberrypi3.raucb`"+` succeeded\n"}`,
		w.Body.String(),
	)
}

func TestStatus(t *testing.T) {
	srv := newTestServer()
	w := srv.testRequest(t, "GET", "/api/status", nil, "")

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	assert.Equal(t,
		`{"installing":false,"percent":0,"message":"","last_error":"","log":""}`,
		w.Body.String(),
	)
}

func TestInfo(t *testing.T) {
	srv := newTestServer()
	w := srv.testRequest(t, "GET", "/api/info", nil, "")

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))

	var info model.SystemInfo
	err := json.Unmarshal(w.Body.Bytes(), &info)
	assert.Nil(t, err)

	assert.Equal(t, "TSGRain", info.RaucCompatible)
	assert.Equal(t, "dev", info.RaucVariant)
	assert.Len(t, info.RaucRootfs, 2)
}

func TestReboot(t *testing.T) {
	srv := newTestServer()
	testfile := "/tmp/sebrauc_reboot_test"
	_ = os.Remove(testfile)

	w := srv.testRequest(t, "POST", "/api/reboot", nil, "")

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	assert.Equal(t,
		`{"success":true,"msg":"System is rebooting"}`,
		w.Body.String(),
	)

	time.Sleep(5100 * time.Millisecond)

	assert.FileExists(t, testfile)
}

func TestAuth(t *testing.T) {
	testfiles := fixtures.GetTestfilesDir()

	cfg := config.GetDefault()
	cfg.Authentication.Enable = true
	cfg.Authentication.PasswdFile = filepath.Join(testfiles, "htpasswd")

	srv := newTestServerCfg(cfg)

	t.Run("fail", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/", nil)
		req.SetBasicAuth("plain", "asdf")
		srv.router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusUnauthorized, w.Code)
		assert.Empty(t, w.Body.String())
		assert.Equal(t, "Basic realm=\"Authorization Required\"",
			w.Header().Get("WWW-Authenticate"))
	})

	t.Run("ok", func(t *testing.T) {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/", nil)
		req.SetBasicAuth("plain", "1234")
		srv.router.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)
		assert.NotEmpty(t, w.Body.String())
		assert.Empty(t, w.Header().Get("WWW-Authenticate"))
	})
}
