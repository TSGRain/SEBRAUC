// SEBRAUC
//
// # REST API for the SEBRAUC firmware updater
//
// ---
// Schemes: http, https
// Version: 0.2.0
// License: MIT
//
// swagger:meta
package server

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"code.thetadev.de/TSGRain/SEBRAUC/src/config"
	"code.thetadev.de/TSGRain/SEBRAUC/src/model"
	"code.thetadev.de/TSGRain/SEBRAUC/src/rauc"
	"code.thetadev.de/TSGRain/SEBRAUC/src/server/middleware"
	"code.thetadev.de/TSGRain/SEBRAUC/src/server/stream"
	"code.thetadev.de/TSGRain/SEBRAUC/src/server/swagger"
	"code.thetadev.de/TSGRain/SEBRAUC/src/sysinfo"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util/mode"
	"code.thetadev.de/TSGRain/SEBRAUC/ui"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type SEBRAUCServer struct {
	config   *config.Config
	streamer *stream.API
	updater  *rauc.Rauc
	sysinfo  *sysinfo.Sysinfo
	tmpdir   string
}

func NewServer(config *config.Config) *SEBRAUCServer {
	updater := rauc.New(config.Commands.RaucInstall)
	streamer := stream.New(
		time.Duration(config.Server.Websocket.Ping)*time.Second,
		time.Duration(config.Server.Websocket.Timeout)*time.Second,
		[]string{},
	)
	sysinfo := sysinfo.New(
		config.Commands.RaucStatus,
		config.Sysinfo.ReleaseFile,
		config.Sysinfo.NameKey,
		config.Sysinfo.VersionKey,
		config.Sysinfo.HostnameFile,
		config.Sysinfo.UptimeFile,
	)

	updater.SetBroadcaster(streamer)

	tmpdir := util.GetTmpdir(config.Tmpdir)

	return &SEBRAUCServer{
		config:   config,
		updater:  updater,
		streamer: streamer,
		sysinfo:  sysinfo,
		tmpdir:   tmpdir,
	}
}

func (srv *SEBRAUCServer) getRouter() *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	_ = router.SetTrustedProxies(nil)

	if mode.IsDev() {
		router.Use(cors.Default())
	}

	router.Use(middleware.ErrorHandler(false), middleware.PanicHandler(false))
	router.NoRoute(func(c *gin.Context) { c.Error(util.ErrPageNotFound) })

	if srv.config.Authentication.Enable {
		router.Use(middleware.Authentication(srv.config.Authentication.PasswdFile))
	}

	api := router.Group("/api",
		middleware.ErrorHandler(true), middleware.PanicHandler(true))

	// API ROUTES
	api.GET("/ws", srv.streamer.Handle)
	api.GET("/status", srv.controllerStatus)
	api.GET("/info", srv.controllerInfo)

	api.POST("/update", srv.controllerUpdate)
	api.POST("/reboot", srv.controllerReboot)

	// Error routes for testing
	if mode.IsDev() {
		router.GET("/error", srv.controllerError)
		router.GET("/panic", srv.controllerPanic)

		api.GET("/error", srv.controllerError)
		api.GET("/panic", srv.controllerPanic)
	}

	// UI
	uiGroup := router.Group("/", middleware.Compression(
		srv.config.Server.Compression.Gzip,
		srv.config.Server.Compression.Brotli),
	)
	ui.Register(uiGroup)
	swagger.Register(uiGroup)

	return router
}

func (srv *SEBRAUCServer) Run() error {
	router := srv.getRouter()

	return router.Run(fmt.Sprintf("%s:%d",
		srv.config.Server.Address, srv.config.Server.Port))
}

// swagger:operation POST /update startUpdate
//
// # Start the update process
//
// ---
// consumes:
// - multipart/form-data
// produces: [application/json]
// parameters:
//   - name: updateFile
//     in: formData
//     description: RAUC firmware image file (*.raucb)
//     required: true
//     type: file
//
// responses:
//
//	200:
//	  description: Ok
//	  schema:
//	    $ref: "#/definitions/StatusMessage"
//	409:
//	  description: already running
//	  schema:
//	    $ref: "#/definitions/Error"
//	500:
//	  description: Server Error
//	  schema:
//	    $ref: "#/definitions/Error"
func (srv *SEBRAUCServer) controllerUpdate(c *gin.Context) {
	file, err := c.FormFile("updateFile")
	if err != nil {
		c.Error(err)
		return
	}

	uid, err := uuid.NewRandom()
	if err != nil {
		c.Error(err)
		return
	}

	updateFile := fmt.Sprintf("%s/update_%s.raucb", srv.tmpdir, uid.String())

	err = c.SaveUploadedFile(file, updateFile)
	if err != nil {
		c.Error(err)
		return
	}

	err = srv.updater.RunRauc(updateFile)
	if err == nil {
		writeStatus(c, true, "Update started")
	} else {
		c.Error(err)
		return
	}
}

// swagger:operation GET /status getStatus
//
// # Get the current status of the RAUC updater
//
// ---
// produces: [application/json]
// responses:
//
//	200:
//	  description: Ok
//	  schema:
//	    $ref: "#/definitions/RaucStatus"
//	500:
//	  description: Server Error
//	  schema:
//	    $ref: "#/definitions/Error"
func (srv *SEBRAUCServer) controllerStatus(c *gin.Context) {
	c.JSON(http.StatusOK, srv.updater.GetStatus())
}

// swagger:operation GET /info getInfo
//
// # Get the current system info
//
// ---
// produces: [application/json]
// responses:
//
//	200:
//	  description: Ok
//	  schema:
//	    $ref: "#/definitions/SystemInfo"
//	500:
//	  description: Server Error
//	  schema:
//	    $ref: "#/definitions/Error"
func (srv *SEBRAUCServer) controllerInfo(c *gin.Context) {
	info, err := srv.sysinfo.GetSysinfo()
	if err != nil {
		c.Error(err)
	} else {
		c.JSON(http.StatusOK, info)
	}
}

// swagger:operation POST /reboot startReboot
//
// # Reboot the system
//
// ---
// produces: [application/json]
// responses:
//
//	200:
//	  description: Ok
//	  schema:
//	    $ref: "#/definitions/StatusMessage"
//	500:
//	  description: Server Error
//	  schema:
//	    $ref: "#/definitions/Error"
func (srv *SEBRAUCServer) controllerReboot(c *gin.Context) {
	go util.Reboot(srv.config.Commands.Reboot, 5*time.Second)

	writeStatus(c, true, "System is rebooting")
}

// controllerError throws an error for testing
func (srv *SEBRAUCServer) controllerError(c *gin.Context) {
	c.Error(util.HttpErrNew("error test", http.StatusBadRequest))
}

// controllerPanic panics for testing
func (srv *SEBRAUCServer) controllerPanic(c *gin.Context) {
	panic(errors.New("panic message"))
}

func writeStatus(c *gin.Context, success bool, msg string) {
	c.JSON(http.StatusOK, model.StatusMessage{
		Success: success,
		Msg:     msg,
	})
}
