package fixtures

import (
	"os"
	"path/filepath"
	"strings"
)

var envPrefixes = []string{"SEBRAUC", "RAUC_MOCK"}

func doesFileExist(filepath string) bool {
	_, err := os.Stat(filepath)
	return !os.IsNotExist(err)
}

func getProjectRoot() string {
	p, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	for i := 0; i < 10; i++ {
		if doesFileExist(filepath.Join(p, "go.mod")) {
			return p
		}
		p = filepath.Join(p, "..")
	}

	panic("Could not find project root")
}

func CdProjectRoot() {
	root := getProjectRoot()
	err := os.Chdir(root)
	if err != nil {
		panic(err)
	}
}

func GetTestfilesDir() string {
	CdProjectRoot()
	return filepath.Join("src", "fixtures", "testfiles")
}

func ResetEnv() {
	for _, envvar := range os.Environ() {
		split := strings.SplitN(envvar, "=", 2)
		if len(split) != 2 {
			continue
		}

		key := split[0]

		for _, prefix := range envPrefixes {
			if strings.HasPrefix(key, prefix) {
				_ = os.Unsetenv(key)
			}
		}
	}
}
