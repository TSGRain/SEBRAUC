package fixtures

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetProjectRoot(t *testing.T) {
	t.Run("default", func(t *testing.T) {
		root := getProjectRoot()
		assert.True(t, doesFileExist(filepath.Join(root, "go.sum")))
	})

	t.Run("subdir", func(t *testing.T) {
		root1 := getProjectRoot()
		err := os.Chdir(filepath.Join(root1, "src/rauc"))
		if err != nil {
			panic(err)
		}

		root := getProjectRoot()
		assert.True(t, doesFileExist(filepath.Join(root, "go.sum")))
	})
}

func TestCdProjectRoot(t *testing.T) {
	CdProjectRoot()
	err := os.Chdir("src/rauc")
	if err != nil {
		panic(err)
	}
	CdProjectRoot()
	assert.True(t, doesFileExist("go.sum"))
}

func TestResetEnv(t *testing.T) {
	os.Setenv("RAUC_MOCK_TEST", "1")
	os.Setenv("SEBRAUC_PORT", "8001")

	ResetEnv()

	_, exists := os.LookupEnv("RAUC_MOCK_TEST")
	assert.False(t, exists)
	_, exists = os.LookupEnv("SEBRAUC_PORT")
	assert.False(t, exists)
}
