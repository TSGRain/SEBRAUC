package main

import (
	"fmt"
	"os"
	"strings"
	"time"
)

const outputSuccess = `installing
0% Installing
0% Determining slot states
20% Determining slot states done.
20% Checking bundle
20% Verifying signature
40% Verifying signature done.
40% Checking bundle done.
40% Checking manifest contents
60% Checking manifest contents done.
60% Determining target install group
80% Determining target install group done.
80% Updating slots
80% Checking slot rootfs.0
90% Checking slot rootfs.0 done.
90% Copying image to rootfs.0
100% Copying image to rootfs.0 done.
100% Updating slots done.
100% Installing done.
idle
Installing ` + "`/app/tsgrain-update-raspberrypi3.raucb` succeeded"

const outputFailure = `installing
0% Installing
0% Determining slot states
20% Determining slot states done.
20% Checking bundle
40% Checking bundle failed.
100% Installing failed.
LastError: Failed to check bundle identifier: Invalid identifier. ` +
	`Did you pass a valid RAUC bundle?
idle
Installing ` + "/app/demo` failed"

const statusJson = `{"compatible":"TSGRain","variant":"dev","booted":"A",` +
	`"boot_primary":"rootfs.0","slots":[{"rootfs.1":{"class":"rootfs",` +
	`"device":"/dev/mmcblk0p3","type":"ext4","bootname":"B","state":"inactive",` +
	`"parent":null,"mountpoint":null,"boot_status":"good"}},{"rootfs.0":` +
	`{"class":"rootfs","device":"/dev/mmcblk0p2","type":"ext4","bootname":"A",` +
	`"state":"booted","parent":null,"mountpoint":"/","boot_status":"good"}}]}`

func printLinesWithDelay(lines string, delay time.Duration) {
	for _, line := range strings.Split(lines, "\n") {
		fmt.Println(line)
		time.Sleep(delay)
	}
}

func getBoolEnvvar(name string) bool {
	val := strings.ToLower(os.Getenv(name))
	return val != "" && val != "false" && val != "0"
}

func main() {
	method := ""
	if len(os.Args) > 1 {
		method = os.Args[1]
	}

	test := getBoolEnvvar("RAUC_MOCK_TEST")
	failure := getBoolEnvvar("RAUC_MOCK_FAIL")

	delay := 500 * time.Millisecond
	if test {
		delay = 10 * time.Millisecond
	}

	switch method {
	case "install":
		if failure {
			printLinesWithDelay(outputFailure, delay)
		} else {
			printLinesWithDelay(outputSuccess, delay)
		}
	case "status":
		if os.Args[2] != "--output-format=json" {
			fmt.Println("output format must be json")
			os.Exit(1)
		}
		fmt.Println(statusJson)
	default:
		fmt.Println("invalid method")
		os.Exit(1)
	}
}
