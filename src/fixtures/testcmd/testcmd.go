package testcmd

//nolint:lll
const (
	RaucStatus  = "go run code.thetadev.de/TSGRain/SEBRAUC/src/fixtures/rauc_mock status --output-format=json"
	RaucInstall = "go run code.thetadev.de/TSGRain/SEBRAUC/src/fixtures/rauc_mock install"
	Reboot      = "touch /tmp/sebrauc_reboot_test"
)
