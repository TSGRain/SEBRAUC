package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"code.thetadev.de/TSGRain/SEBRAUC/src/config"
	"code.thetadev.de/TSGRain/SEBRAUC/src/server"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util/mode"
)

const titleArt = `   _____ __________  ____  ___   __  ________
  / ___// ____/ __ )/ __ \/   | / / / / ____/
  \__ \/ __/ / __  / /_/ / /| |/ / / / /
 ___/ / /___/ /_/ / _, _/ ___ / /_/ / /___
/____/_____/_____/_/ |_/_/  |_\____/\____/  `

func main() {
	run(os.Args[1:])
}

func run(args []string) {
	fmt.Println(titleArt + util.Version() + "\n")

	cmdFlags := flag.NewFlagSet("sebrauc", flag.ExitOnError)
	port := cmdFlags.Int("p", 0, "HTTP port")
	cfgPath := cmdFlags.String("c", "", "Config file path")
	_ = cmdFlags.Parse(args)

	if mode.IsDev() {
		fmt.Println("Test mode active - no update operations are executed.")
		fmt.Println("Build with -tags prod to enable live mode.")
	}

	cfg := config.GetWithFlags(*cfgPath, *port)

	fmt.Printf("Starting server at %s:%d\n", cfg.Server.Address, cfg.Server.Port)

	srv := server.NewServer(cfg)
	err := srv.Run()
	if err != nil {
		log.Fatalln(err)
	}
}
