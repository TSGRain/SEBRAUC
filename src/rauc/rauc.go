package rauc

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"sync"

	"code.thetadev.de/TSGRain/SEBRAUC/src/model"
	"code.thetadev.de/TSGRain/SEBRAUC/src/util"
)

var (
	expPercentage = regexp.MustCompile(`^ *(\d+)% +(.+)`)
	expLastError  = regexp.MustCompile(`^LastError: +(.+)`)
)

type Rauc struct {
	cmdRaucInstall string
	bc             util.Broadcaster
	status         model.RaucStatus
	runningMtx     sync.Mutex
}

func New(cmdRaucInstall string) *Rauc {
	return &Rauc{
		cmdRaucInstall: cmdRaucInstall,
	}
}

func (r *Rauc) SetBroadcaster(bc util.Broadcaster) {
	r.bc = bc
	r.bcStatus()
}

func (r *Rauc) completed(updateFile string) {
	r.status.Installing = false
	r.bcStatus()

	_ = os.Remove(updateFile)
}

func (r *Rauc) RunRauc(updateFile string) error {
	// Check file input
	if !util.DoesFileExist(updateFile) {
		return util.ErrFileDoesNotExist
	}

	// Only run one instance at a time
	r.runningMtx.Lock()
	isRunning := r.status.Installing
	r.status.Installing = true
	r.runningMtx.Unlock()

	if isRunning {
		return util.ErrAlreadyRunning
	}

	// Reset installer
	r.status = model.RaucStatus{
		Installing: true,
	}
	r.bcStatus()

	cmd := util.CommandFromString(r.cmdRaucInstall + " " + updateFile)

	readPipe, _ := cmd.StdoutPipe()
	cmd.Stderr = cmd.Stdout

	scanner := bufio.NewScanner(readPipe)

	go func() {
		for scanner.Scan() {
			line := scanner.Text()
			hasUpdate := false

			if line != "idle" && line != "installing" {
				r.status.Log += line + "\n"
			}

			match := expPercentage.FindStringSubmatch(line)
			if match != nil {
				r.status.Percent, _ = strconv.Atoi(match[1])
				r.status.Message = match[2]
				hasUpdate = true
			} else {
				match = expLastError.FindStringSubmatch(line)
				if match != nil {
					r.status.LastError = match[1]
					hasUpdate = true
				}
			}

			if hasUpdate {
				r.bcStatus()
			}
		}
	}()

	err := cmd.Start()
	if err != nil {
		r.completed(updateFile)
		return err
	}

	go func() {
		err := cmd.Wait()
		if err != nil {
			fmt.Printf("RAUC failed with %s\n", err)
		}
		r.completed(updateFile)
	}()

	return nil
}

func (r *Rauc) GetStatus() model.RaucStatus {
	return r.status
}

func (r *Rauc) GetStatusJson() []byte {
	statusJson, err := json.Marshal(r.status)
	if err != nil {
		return []byte{}
	}

	return statusJson
}

func (r *Rauc) bcStatus() {
	r.bc.Broadcast(r.GetStatusJson())
}
