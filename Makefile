SRC_DIR=./src
UI_DIR=./ui

APIDOC_FILE=./src/server/swagger/swagger.yaml

VER=$(or ${VERSION},$(shell git tag --sort=-version:refname | head -n 1))

setup:
	cd ${UI_DIR} && pnpm install

test:
	go test -v ./src/...

lint:
	golangci-lint run
	cd ${UI_DIR} && npm run format && npm run lint

build-ui:
	cd ${UI_DIR} && pnpm run build

build-server:
	go build -tags prod -ldflags "-s -w -X code.thetadev.de/TSGRain/SEBRAUC/src/util.version=${VER}" -o build/sebrauc ./src/.

build: build-ui build-server

generate-apidoc:
	SWAGGER_GENERATE_EXTENSION=false swagger generate spec --scan-models -o ${APIDOC_FILE}

generate-apiclient:
	openapi-generator generate -i ${APIDOC_FILE} -g typescript-axios -o ${UI_DIR}/src/sebrauc-client -p "supportsES6=true"
	cd ${UI_DIR} && npm run format

clean:
	rm -f build/*
	rm -rf ${UI_DIR}/dist/**
